<?php
$sessionId = session_id();

if(empty($sessionId)) {
     @session_start();
}

$providerGet = null;
if (!empty($_GET['provider'])) {
     $providerGet = $_GET['provider'];
     $_SESSION['provider'] = $providerGet;
} else if (!empty($_SESSION['provider'])) {
     $providerGet = $_SESSION['provider'];
}
?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Program do zarządzania magazynem | szybkafaktura.pl</title>
        <meta name="keywords" content="faktura online, fakturowanie online, faktura vat, faktura vat online" />
<meta name="description" content="Magazyn w szybkafaktura.pl jest doskonale przystosowany do potrzeb małych i średnich firm handlowych np. sklepów. " />
        <meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="canonical" href="http://szybkafaktura.pl/magazyn.php" />
				<?php include_once("includes/head.php") ?>
        <link rel="stylesheet" href="css/magnific.css">

    </head>
    <body class="product-page" id="zarzadzaj-magazynem">

				<?php include_once("includes/header.php") ?>

				<section id="intro">
					
					<div class="container">
						<div class="inside">
						
							<h1>Program do zarządzania magazynem</h1>
							
							<p>Magazyn w szybkafaktura.pl jest doskonale przystosowany do potrzeb<br class="hide-on-phone">małych i średnich firm handlowych np. sklepów.</p>
							<p>Automatyczne tworzenie dokumentów księgowo-magazynowych usprawni pracę Twojej firmy<br class="hide-on-phone">
								i pozwoli na bardziej efektywne działanie. Obsługa programu magazynowego<br class="hide-on-phone">
								jeszcze nigdy nie była tak prosta!</p>

							
							<div id="laptop">
								
								<div id="desktop">
								
									<a id="front-screen" href="screens/magazyn/5.png">
										<img src="screens/magazyn/5.png"  alt="Program do zarządzania magazynem"  />
										<span class="zoom1">&nbsp;</span>
										<span class="zoom2">&nbsp;</span>
									</a>
									<a id="second-screen" href="screens/magazyn/6.png">
										<img src="screens/magazyn/6.png"  alt="Program do zarządzania magazynem" />
										<span class="zoom1">&nbsp;</span>
										<span class="zoom2">&nbsp;</span>
									</a>
								
								</div>
								
							</div>

						
						</div>
					</div>
					
				</section>
				
				<section id="features">
					
					<div class="container">
						<div class="inside">
						
							<div class="feature" id="latwosc-obslugi">
								
								
								<figure><img src="img/image-4.svg" alt="Program do zarządzania magazynem" ></figure>

								<div class="text">
																
									<h2>Intuicyjna obsługa</h2>
									<p>szybkafaktura.pl zapewnia niezbędną funkcjonalność do&nbsp;obsługi gospodarki magazynowej. Prosta obsługa sprawia, że&nbsp;zarządzanie magazynem jest szybkie i&nbsp;bezproblemowe. </p>
								
								</div>
						
							</div>

							<div class="feature" id="automatyczne-przetwarzanie">
																
								<figure class="show-on-phone"><img src="img/image-5.svg" alt="Intuicyjna obsługa" ></figure>

								<div class="text">
									<h2>Stała kontrola stanów magazynowych</h2>
									<p>Program pozwala na&nbsp;bieżąco kontrolować stany magazynowe w&nbsp;Twojej firmie. W każdej chwili możesz sprawdzić, które towary są na&nbsp;wyczerpaniu i&nbsp;od&nbsp;razu skontaktować się ze&nbsp;swoim dostawcą.</p>
								
								</div>

								<figure class="hide-on-phone"><img src="img/image-5.svg" alt="kontrola stanów magazynowych" ></figure>
						
							</div>

							<div class="feature" id="kontrola-finansow">
																
								<figure><img src="img/image-6.svg" alt="Program do zarządzania magazynem" ></figure>

								<div class="text">

									<h2>Sprawne zarządzanie dokumentami</h2>
									<p>Nie musisz już pamiętać o&nbsp;wystawianiu dokumentów magazynowych. W&nbsp;szybkafaktura.pl proces tworzenia dokumentów jest w&nbsp;pełni zautomatyzowany, a&nbsp;inwentaryzacje niezwykle łatwe do&nbsp;przeprowadzenia.</p>
									
								</div>
						
							</div>

						
						</div>
					</div>
					
				</section>
				
				<section id="signup">
					
					<div class="container">
						<div class="inside">
							<p>Dołącz do grona zadowolonych klientów.</p><a href="https://app.szybkafaktura.pl/auth/new-register?app=skto<?php echo ($providerGet !== null) ? '&provider='.$providerGet : '' ?>" class="medium button green signup newButtonsignup">Załóż konto</a><p>Wypróbuj przez 45 dni za darmo!</p>
						</div>
					</div>
					
				</section>
				
				<section id="details">
					
					<div class="container">
						<div class="inside">
						
							<h2>Najważniejsze funkcje</h2>
							
							<div class="row">
							
							<ul class="column">
								<li>możliwość importu listy towarów</li>
								<li>tworzenie bazy asortymentu i kontrahentów</li>
								<li>wystawianie dokumentów sprzedaży (faktur)</li>
								<li>wystawianie dokumentów PZ, WZ, PW, RW</li>
								<li>tworzenie korekt dokumentów magazynowych</li>
								<li>generowanie JPK_MAG</li>
							</ul>
							
							<ul class="column">
								<li>łatwe przeglądanie, wyszukiwanie, sortowanie dokumentów</li>
								<li>możliwość wykonania inwentaryzacji</li>
								<li>drukowanie i wysyłanie dokumentów magazynowych</li>
								<li>automatyczne wystawianie dokumentów magazynowych do dokumentów księgowych</li>
							</ul>
							
							</div>
						
						</div>
					</div>					
					
				</section>

				<section id="quotes">
					
					<div class="container">
						<div class="inside">
						
							<ul class="quotes cycle-slideshow" data-cycle-timeout="2000" data-cycle-slides="> li.quote">
								
								<li class="quote">
									<span class="quote-content">Prowadzę sklep internetowy, więc nie potrzebuję rozbudowanych programów magazynowych.<br>
									szybkafaktura.pl ma wszystko to co potrzebuję co lubię - prosty w obsłudze i funkcjonalny program do fakturowania i obsługi mojego magazynu.</span>
									<span class="quote-author">Michał, właściciel sklepu internetowego</span>
								</li>

						    <div class="cycle-pager"></div>		

							</ul>
						
						</div>
					</div>					
					
				</section>

				<?php include_once("includes/footer.php") ?>

        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='//www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-1326744-3');
			ga('set', 'contentGroup1', 'Grupa www kod'); 
			ga('send','pageview');
        </script>
    </body>
</html>
