<?php
$sessionId = session_id();

if(empty($sessionId)) {
      @session_start();
}

$providerGet = null;
if (!empty($_GET['provider'])) {
     $providerGet = $_GET['provider'];
     $_SESSION['provider'] = $providerGet;
} else if (!empty($_SESSION['provider'])) {
     $providerGet = $_SESSION['provider'];
}
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Wszystkie funkcje - SzybkaFaktura.pl</title>
        <meta name="keywords" content="faktura online, fakturowanie online, faktura vat, faktura vat online" />
<meta name="description" content="Wszystkie funkcjonalności systemu szybkafaktura.pl" />
        <meta name="viewport" content="width=device-width, initial-scale=1">


				<?php include_once("includes/head.php") ?>
        <link rel="stylesheet" href="css/jquery.tooltipster.css">
        
    </head>
    <body class="product-page" id="cennik">

				<?php include_once("includes/header.php") ?>

				<div class="container">
					<div class="inside">
											
						<h1>Lista wszystkich funkcjonalności systemu</h1>
						

					<section id="pricelist" class="hide show-on-portrait show-on-tablet show-on-desktop">
											
							
						<div id="table-header">								

							<ul id="header-names">
								
								<li class="pakiet-podstawowy">Pakiet<br>Podstawowy</li>
								<li class="pakiet-pelny">Pakiet<br>Magazyn</li>
								<li class="pakiet-all-inclusive">Pakiet<br>Pełny</li>
								
							</ul>
							
						
							
						</div>
						<br><br><br><br>
				
                <div id="acordion"><ul>
  <li>
    <input type="checkbox" checked />
    <i></i>
    <h2>Fakturowanie online</h2>
    <p><section id="pricelist">
        <table>
							<tr>
								<td>Faktura VAT, proforma, zaliczkowa, VAT marża</td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
							</tr>
							<tr>
								<td>Faktura wewnątrzwspólnotowa</td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
							</tr>
							<tr>
								<td>Korekta, przychód VAT, paragon</td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
							</tr>
							<tr>
								<td>Faktury w walucie obcej według tabeli NBP</td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
							</tr>
							<tr>
								<td>Faktury cykliczne </td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
							</tr>
							<tr>
								<td>JPK_FA - (Jednolity plik konktrolny)</td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
							</tr>
							<tr>
								<td>Zautomatyzowana wysyłka faktur pocztąs</td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
							</tr>
                            <tr>
								<td>Wielojęzyczne faktury <em class="tooltip" title="PL, EN, FR, DE">więcej</em></td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
							</tr>
                            <tr>
								<td>Statusy faktur</td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
							</tr>
							<tr>
								<td>Historia dokumentu <em class="tooltip" title="Rejestr opreacji wykonanych na dokumencie">więcej</em></td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
							</tr>
                            <tr>
								<td>Baza kontrahentów z możliwością importu i historią kontaktu</td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
							</tr>
                            <tr>
								<td>Definiowanie indywidualnych warunków handlowych</td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
							</tr>
                            <tr>
								<td>Baza asortymentu z podziałem na towary i usługi</td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
							</tr>
                            <tr>
								<td>Dokumenty kosztowe <em class="tooltip" title="faktura VAT, rozchód VAT, nota korygująca, dowód wewnętrzny">więcej</em></td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
							</tr>
                            <tr>
								<td>Tworzenie serii numeracji</td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
							</tr>
                            <tr>
								<td>Tworzenie tagów dla dokumentów</td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
							</tr>
                            <tr>
								<td>Automatyczne pobieranie kursu z NBP</td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
							</tr>
                            <tr>
								<td>Automatycznie tworzenie raportów <em class="tooltip" title="drukowanie do pdf i xls">Więcej</em></td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
							</tr>
                            <tr>
								<td>Logo firmy na fakturze</td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
							</tr>
                            <tr>
								<td>Indywidualne numeracje dla dokumentów </td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
							</tr>
                           
                            <tr>
								<td>Automatyczne rozpoznawanie danych z dokumentów (OCR)</td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
							</tr>
                            <tr>
								<td>Pobieranie danych kontrahenta z GUS i VIES</td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
							</tr>
                            <tr>
								<td>Pieczęć prewencyjna na fakturze</td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
							</tr>
                            <tr>
								<td>Wiele rachunków bankowych na fakturze</td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
							</tr>
							<tr>
								<td>Weryfikacja rachunku bankowego na tzw. białej liście podatników MF</td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
							</tr>
                            <tr>
								<td>Funkcja masowego wystawiania faktur <em class="tooltip" title="dla kilku kontrahentów naraz">Więcej</em></td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
							</tr>
                            <tr>
								<td>Zbiorcze drukowanie i wysyłanie dokumentów</td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
							</tr>
                            <tr>
								<td>Import i eksport dokumentów w wielu formatach</td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
							</tr>
                            <tr>
								<td>Elektroniczna wysyłka faktur ze śledzeniem statusu</td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
							</tr>
                            <tr>
								<td>Rozbudowane możliwości udostępniania dokumentów</td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
							</tr>
                            <tr>
								<td>Dodatkowe konta użytkowników bez limitu</td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
							</tr>
                            <tr>
								<td>Szablony dokumentów</td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
							</tr>
                           
                            <tr>
								<td>Aplikacja mobilna <a href="https://itunes.apple.com/pl/app/szybkafaktura.pl/id1049729347?mt=8">iOS</a> / <a href="https://play.google.com/store/apps/details?id=com.cp.szybka&hl=pl">Android</a></td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
							</tr>
                            <tr>
								<td>Wysyłka faktur pocztą</td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
							</tr>
                            
                            
                            </table></section></p>
  </li>
  <li>
    <input type="checkbox" checked>
    <i></i>
    <h2>Należności i zobowiązania</h2>
    <p><section id="pricelist">
        <table>
							<tr>
								<td>Automatycznie tworzone zestawienia należności<div class="new">Nowość</div></td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
							</tr>
							<tr>
								<td>Dodawanie płatności dla dokumentów</td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
							</tr>
							<tr>
								<td>Wysyłanie przypomnień o zapłacie dokumentu do kontrahenta</td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
							</tr>
                            <tr>
								<td>Tworzenie wezwań do zapłaty</td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
							</tr>
							<tr>
								<td>Import wyciągów bankowych <em>plik MT-940</em></td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
							</tr>
							<tr>
								<td>Eksport danych do przelewu w standardzie PLI</td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
							</tr>
							</table></section></p>
  </li>

<li>
    <input type="checkbox" checked>
    <i></i>
    <h2>Księgowość online</h2>
    <p><section id="pricelist">
        <table>
							<tr>
								<td>Przypomnienia mailowe o terminach<em class="tooltip" title="ZUS, PIT i VAT">Więcej</em></td>
								<td><em class="yes">tak</em></td>
								<td></td>
								<td><em class="yes">tak</em></td>
							</tr>
							<tr>
								<td>Księgowanie i odksięgowywanie dokumentów</td>
								<td></td>
								<td></td>
								<td><em class="yes">tak</em></td>
							</tr>
							<tr>
								<td>Kategorie i schematy księgowe</td>
								<td></td>
								<td></td>
								<td><em class="yes">tak</em></td>
							</tr>
							<tr>
								<td>Rejestr sprzedaży i zakupu VAT<em>pdf., xls.</em></td>
								<td></td>
								<td></td>
								<td><em class="yes">tak</em></td>
							</tr>
							<tr>
								<td>KPiR - księga przychodów i rozchodów<em>pdf., xls.</em></td>
								<td></td>
								<td></td>
								<td><em class="yes">tak</em></td>
							</tr>
                            <tr>
								<td>Roczny raport KPiR</td>
								<td></td>
								<td></td>
								<td><em class="yes">tak</em></td>
							</tr>
							<tr>
								<td>JPK_VAT - (Jednolity plik konktrolny)</td>
								<td></td>
								<td></td>
								<td><em class="yes">tak</em></td>
							</tr>
							<tr>
								<td>JPK_KPiR</td>
								<td></td>
								<td></td>
								<td><em class="yes">tak</em></td>
							</tr>
							
                            <tr>
								<td>Raport dochodu w roku podatkowym</td>
								<td></td>
								<td></td>
								<td><em class="yes">tak</em></td>
							</tr>
                            <tr>
								<td>Deklaracja VAT-7 i VAT-7K</td>
								<td></td>
								<td></td>
								<td><em class="yes">tak</em></td>
							</tr>
                            <tr>
								<td>Deklaracja VAT-UE</td>
								<td></td>
								<td></td>
								<td><em class="yes">tak</em></td>
							</tr>
                            <tr>
								<td>Deklaracja PIT-5 i PIT-5L<em>miesięczne i kwartalne</em></td>
								<td></td>
								<td></td>
								<td><em class="yes">tak</em></td>
							</tr>
                            <tr>
								<td>Amortyzacja środków trwałych</td>
								<td></td>
								<td></td>
								<td><em class="yes">tak</em></td>
							</tr>
                            <tr>
								<td>Automatyczne tworzenie odpisów amortyzacyjnych</td>
								<td></td>
								<td></td>
								<td><em class="yes">tak</em></td>
							</tr>
                            <tr>
								<td>Pojazdy</td>
								<td></td>
								<td></td>
								<td><em class="yes">tak</em></td>
							</tr>
                            <tr>
								<td>Kilometrówka</td>
								<td></td>
								<td></td>
								<td><em class="yes">tak</em></td>
							</tr>
                            
							</table></section></p>
  </li>
  
  <li>
    <input type="checkbox" checked>
    <i></i>
    <h2>OCR - wprowadzanie danych</h2>
    <p><section id="pricelist">
        <table>
							<tr>
								<td>Rozpoznawanie danych<em class="tooltip" title="PDF, JPG, PNG, TIFF">Więcej</em></td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
							</tr>
							<tr>
								<td>Dodawanie dokumentów<em class="tooltip" title="mail, drag&drop, hot folder.">Więcej</em></td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
							</tr>
                            <tr>
								<td>Zaawansowana weryfikacja danych<em class="tooltip" title="ostrzerzenia systemowe, praca na skanie dokumentu.">Więcej</em></td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
							</tr>
                            <tr>
								<td>Moduł eksportu danych do systemów finansowo księgowych</td>
								<td></td>
								<td></td>
								<td><em class="yes">tak</em></td>
							</tr>
                            <tr>
								<td>Aplikacja mobilna z funkcją OCR <em><a href="https://itunes.apple.com/pl/app/szybkafaktura.pl/id1049729347?mt=8">iOS</a> / <a href="https://play.google.com/store/apps/details?id=com.cp.szybka&hl=pl">Android</a></em></td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
							</tr>
                            <tr>
								<td>Repozytorium plików źródłowych</td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
							</tr>
                            <tr>
								<td>Funkcja automatycznego podziału dokumentów</td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
							</tr>
                            <tr>
								<td>Funkcja automatycznego eksportu dokumentów</td>
								<td></td>
								<td></td>
								<td><em class="yes">tak</em></td>
							</tr>
                           
							</table></section></p>
  </li>

<li>
    <input type="checkbox" checked>
    <i></i>
    <h2>Deklaracje podatkowe</h2>
    <p><section id="pricelist">
        <table>
						
							<tr>
								<td>Ewidencja VAT sprzedaży i zakupu<em class="tooltip" title="miesięcznie lub kwartalnie z możliwością drukowania i wysyłania (pdf, xls)">Więcej</em></td>
								<td></td>
								<td></td>
								<td><em class="yes">tak</em></td>
							</tr>
							<tr>
								<td>Deklaracje podatkowe<em class="tooltip" title="VAT-7, VAT-UE, PIT-5, PIT-4R, PIT-11">więcej</em></td>
								<td></td>
								<td></td>
								<td><em class="yes">tak</em></td>
							</tr>
							<tr>
								<td>Deklaracje ZUS z eksportem do programu Płatnik<em class="tooltip" title="DRA, RCA, ZUA, ZZA, ZWUA">więcej</em></td>
								<td></td>
								<td></td>
								<td><em class="yes">tak</em></td>
							</tr>
							<tr>
								<td>E-deklaracje</td>
								<td></td>
								<td></td>
								<td><em class="yes">tak</em></td>
							</tr>
                            
							</table></section></p>
  </li>

<li>
    <input type="checkbox" checked>
    <i></i>
    <h2>Kadry i płace</h2>
    <p><section id="pricelist">
        <table>
							<tr>
								<td>Rejestr pracowników</td>
								<td></td>
								<td></td>
								<td><em class="yes">tak</em></td>
							</tr>
							<tr>
								<td>Rozliczanie umów o prace, zlecenia i o dzieło</td>
								<td></td>
								<td></td>
								<td><em class="yes">tak</em></td>
							</tr>
							<tr>
								<td>Tworzenie umów dla pracowników</td>
								<td></td>
								<td></td>
								<td><em class="yes">tak</em></td>
							</tr>
							<tr>
								<td>Rachunki do umów cywilnoprawnych</td>
								<td></td>
								<td></td>
								<td><em class="yes">tak</em></td>
							</tr>
							<tr>
								<td>Ewidencja czasu pracy</td>
								<td></td>
								<td></td>
								<td><em class="yes">tak</em></td>
							</tr>
                            <tr>
								<td>Listy płac</td>
								<td></td>
								<td></td>
								<td><em class="yes">tak</em></td>
							</tr>
                            <tr>
								<td>Delegacje</td>
								<td></td>
								<td></td>
								<td><em class="yes">tak</em></td>
							</tr>
                            <tr>
								<td>Deklaracja PIT-11</td>
								<td></td>
								<td></td>
								<td><em class="yes">tak</em></td>
							</tr>
                            <tr>
								<td>Deklaracja PIT-4R</td>
								<td></td>
								<td></td>
								<td><em class="yes">tak</em></td>
							</tr>
                            <tr>
								<td>Deklaracje ZUS z eksportem do programu Płatnik <em class="tooltip" title="DRA, RCA, ZUA, ZZA, ZWUA ">więcej</em></td>
								<td></td>
								<td></td>
								<td><em class="yes">tak</em></td>
							</tr>
							</table></section></p>
  </li>
  
  <li>
    <input type="checkbox" checked>
    <i></i>
    <h2>Magazyn</h2>
    <p><section id="pricelist">
        <table>
							<tr>
								<td>Tworzenie bazy produktów</td>
								<td></td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
							</tr>
							<tr>
								<td>Wystawianie dokumentów PZ, WZ, PW, RW</td>
								<td></td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
							</tr>
                            <tr>
								<td>Tworzenie korekt dokumentów magazynowych</td>
								<td></td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
							</tr>
                            <tr>
								<td>Automatyczne monitorowanie stanów z pełną historią</td>
								<td></td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
							</tr>
                            <tr>
								<td>Kody produktów</td>
								<td></td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
							</tr>
                            <tr>
								<td>Grupy cenowe</td>
								<td></td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
							</tr>
                            <tr>
								<td>Łatwe przeglądanie, wyszukiwanie, sortowanie dokumentów</td>
								<td></td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
							</tr>
                            <tr>
								<td>Inwentaryzacja</td>
								<td></td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
							</tr>
                           <tr>
								<td>Drukowanie i wysyłanie dokumentów magazynowych</td>
								<td></td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
							</tr>
                            <tr>
								<td>Automatyczne wystawianie dokumentów magazynowych</td>
								<td></td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
							</tr>
							<tr>
								<td>JPK_MAG</td>
								<td></td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
							</tr>
							
							</table>
  </li>
    <li>
    <input type="checkbox" checked>
    <i></i>
    <h2>Bezpieczeństwo</h2>
    <p><section id="pricelist">
        <table>
							<tr>
								<td>Bezpłatne aktualizacje</td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
							</tr>
							<tr>
								<td>Kopie zapasowe</td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
							</tr>
                            <tr>
								<td>Bezpieczna transmisja danych <em>SSL</em></td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
							</tr>
                            <tr>
								<td>Powierzenie przetwarzania danych osobowych</td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
							</tr>
                            <tr>
								<td>System zgodny z regulacjami GIODO</td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
							</tr>
                            
							</table></section></p>
  </li>
  
     <li>
    <input type="checkbox" checked>
    <i></i>
    <h2>Pozostałe ważne funkcje</h2>
    <p><section id="pricelist">
        <table>
							<tr>
								<td>Obsługa wielu firm przez jednego użytkownika</td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
							</tr>
                            <tr>
								<td>Workflow<em class="tooltip" title="obieg dokumentów i tworzenie zadań dla użytkowników"/>więcej</em></td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
							</tr>
                            <tr>
								<td>API <div class="new">Nowość</div></td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
							</tr>
                            <tr>
								<td>Eksport do programów księgowych<em class="tooltip" title="Comarch Optima, Symfonia, Raks SQL, Lefthand, Asseco FakiR,KaPeR, Insert Rachmistrz, Insert Rewizor, Fakt, Reset2, Tema">Więcej</em></td>
								<td></td>
								<td></td>
								<td><em class="yes">tak</em></td>
							</tr>
                            <tr>
								<td>Import danych z programów księgowych <em class="tooltip" title="baza kontrahentów, towarów, rejestrów, kont księgowych, kategorii wydatków">więcej</em></td>
								<td></td>
								<td></td>
								<td><em class="yes">tak</em></td>
							</tr>
                            
                            <tr>
								<td>Wsparcie techniczne</td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
								<td><em class="yes">tak</em></td>
							</tr>
                            
                            <tr>
								<td>Opcjonalny branding systemu <em class="tooltip" title="własna domena, kolorystyka, nagłówek, stopka oraz styl maili (dodatkowo płatne, nie jest w żadnym pakiecie standardowo)">więcej</em></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
                            
							</table></section></p>
  </li>

</ul></div></div>
</div>
			
				<?php include_once("includes/footer.php") ?>
				
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='//www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-1326744-3');ga('send','pageview');
        </script>
    </body>
</html>
