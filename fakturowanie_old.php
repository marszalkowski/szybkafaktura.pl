<?php
$sessionId = session_id();

if(empty($sessionId)) {
      @session_start();
}

$providerGet = null;

//var_dump($_SESSION);

if (!empty($_GET['provider'])) {
     $providerGet = $_GET['provider'];
     $_SESSION['provider'] = $providerGet;
     
//     var_dump($providerGet);
     
//     var_dump(1);
} else if (!empty($_SESSION['provider'])) {
     $providerGet = $_SESSION['provider'];
//     var_dump(2);
}
?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Wystawianie faktur  VAT, Fakturowanie Online | szybkafaktura.pl</title>
        <meta name="keywords" content="faktura online, fakturowanie online, faktura vat, faktura vat online" />
<meta name="description" content="System szybkafaktura.pl umożliwia wystawianie dokumentów sprzedażowych i rejestrowanie dokumentów
								kosztowych, jakich potrzebuje mikro i mały przedsiębiorca do prowadzenia swojego biznesu." />
        <meta name="viewport" content="width=device-width, initial-scale=1">

				<?php include_once("includes/head.php") ?>


    </head>
    <body class="product-page" id="wystawiaj-faktury">

				<?php include_once("includes/header.php") ?>

				<section id="intro">
					
					<div class="container">
						<div class="inside">
						
							<h1>Wystawianie faktur VAT</h1>
							
							<p>System szybkafaktura.pl umożliwia wystawianie dokumentów sprzedażowych i rejestrowanie dokumentów<br class="hide-on-mobile">
								kosztowych, jakich potrzebuje mikro i mały przedsiębiorca do prowadzenia swojego biznesu.</p>
							<p>Zaledwie w kilka chwil sporządzisz przejrzysty i estetyczny<br class="hide-on-mobile">
								 dokument, zgodny z obowiązującymi przepisami.</p>

							<div class="signup-first"><a href="https://app.szybkafaktura.pl/auth/register?app=skto<?php echo ($providerGet !== null) ? '&provider='.$providerGet : '' ?>"><button class="big button green button-45-days">Wystaw pierwszą fakturę</button></a></div>
							
							<div id="laptop">
								
								<div id="desktop">

									<a id="front-screen" href="screens/fakturowanie/1.png">
										<img src="screens/fakturowanie/1.png"  alt="Wystawianie faktur VAT" />
										<span class="zoom1">&nbsp;</span>
										<span class="zoom2">&nbsp;</span>
									</a>
									<a id="second-screen" href="screens/fakturowanie/2.png">
										<img src="screens/fakturowanie/2.png"  alt="Wystawianie faktur VAT" />
										<span class="zoom1">&nbsp;</span>
										<span class="zoom2">&nbsp;</span>
									</a>
									
								</div>
								
							</div>

						
						</div>
					</div>
					
				</section>
				
				<section id="features">
					
					<div class="container">
						<div class="inside">
						
							<div class="feature" id="latwosc-obslugi">
								
								<figure><img src="img/image-1.svg" alt="" /></figure>
								
								<div class="text">
																
									<h2>Łatwość obsługi</h2>
									<p>Aby wpisać dane kontrahenta wystarczy podać jego NIP, a&nbsp;pozostałe dane zostaną automatycznie uzupełnione. Gotowy dokument możesz zapisać jako plik pdf lub bezpośrednio z&nbsp;programu przesłać na&nbsp;adres mailowy klienta. Wprowadzone informacje zapisują się w&nbsp;bazie kontrahentów, towarów i&nbsp;usług.</p>
									
								</div>
						
							</div>

							<div class="feature" id="automatyczne-przetwarzanie">
								
								<figure class="show-on-phone"><img src="img/image-2.svg" alt="Dokument sprzedażowy" /></figure>

								<div class="text">
								
									<h2><small>Unikalne rozwiązanie</small><br>Automatyczne przetwarzanie i&nbsp;rozpoznawanie danych z&nbsp;zeskanowanych dokumentów Twojej&nbsp;firmy.</h2>
									<p>System rozpoznaje dane na podstawie skanów dokumentów, co&nbsp;znacznie skraca proces wprowadzania ich do&nbsp;systemu.  Wystarczy zeskanować fakturę lub zrobić zdjęcie za&nbsp;pomocą aplikacji mobilnej, sprawdzić poprawność odczytanych przez system danych i&nbsp;gotowe!</p>
									
								</div>

								<figure class="hide-on-phone"><img src="img/image-2.svg" alt="Dokument sprzedażowy" /></figure>
								
							</div>

							<div class="feature" id="kontrola-finansow">
																
								<figure><img src="img/image-3.svg" alt="Kontrola finansów firmy" /></figure>

								<div class="text">

									<h2>Kontrola finansów firmy</h2>
									<p>Rejestr płatności umożliwia bieżące kontrolowanie Twoich kosztów i&nbsp;faktur a&nbsp;do&nbsp;tych, którzy zalegają z&nbsp;zapłatą, system automatycznie wyśle przypomnienie mailowe.</p>
									
								</div>
						
							</div>

						
						</div>
					</div>
					
				</section>
				
				<section id="signup">
					
					<div class="container">
						<div class="inside">
							<p>Dołącz do grona zadowolonych klientów.</p><a href="https://app.szybkafaktura.pl/auth/register?app=skto<?php echo ($providerGet !== null) ? '&provider='.$providerGet : '' ?>" class="medium button green signup newButtonsignup">Załóż konto</a><p>Wypróbuj przez 45 dni za darmo!</p>
						</div>
					</div>
					
				</section>
				
				<section id="details">
					
					<div class="container">
						<div class="inside">
						
							<h2>Najważniejsze funkcje</h2>
							
							<div class="row">
							
							<ul class="column">
								<li>faktura VAT, proforma, zaliczkowa, VAT marża</li>
								<li>faktura wewnątrzwspólnotowa</li>
								<li>korekta, przychód, przychód VAT, paragony</li>
								<li>faktury w walucie obcej</li>
								<li>import kontrahentów, listy towarów i usług z pliku CSV</li>
								<li>dokumenty kosztowe (faktura VAT, rozchód VAT, rozchód nota korygująca, dowód wewnętrzny)</li>
							</ul>
							
							<ul class="column">
								<li>automatycznie pobieranie kursu z NBP</li>
								<li>automatycznie tworzenie raportów z możliwością drukowania i wysyłania (pdf i xls)</li>
								<li>logo firmy na fakturze</li>
								<li>indywidualne numeracje dla poszczególnych rodzajów dokumentów i dodatkowych użytkowników</li>
								<li>przypomnienia mailowe o terminach składek ZUS, podatek PIT i VAT</li>
								<li>automatyczne rozpoznawanie danych z dokumentów (OCR)</li>
							</ul>
							
							</div>


						
						</div>
						<div class="signup-first"><a href="https://app.szybkafaktura.pl/auth/register?app=skto<?php echo ($providerGet !== null) ? '&provider='.$providerGet : '' ?>"><button class="big button green button-45-days">Wystaw pierwszą fakturę</button></a></div>
					</div>					
					
				</section>

				<section id="quotes">
					
					<div class="container">
						<div class="inside">
						
							<ul class="quotes cycle-slideshow" data-cycle-timeout="2000" data-cycle-slides="> li.quote">
								
								<li class="quote">
									<span class="quote-content">szybkafaktura.pl nazwa mówi sama za siebie - niezawodny serwis do wystawienia faktury online. <br>Polecam wszystkim, którzy cenią sobie profesjonalizm.</span>
									<span class="quote-author">Bartek, informatyk</span>
								</li>

						    <div class="cycle-pager"></div>		

							</ul>
						
						</div>
					</div>					
					
				</section>

				<?php include_once("includes/footer.php") ?>

        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='//www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-1326744-3');
			ga('set', 'contentGroup1', 'Grupa www kod'); 
			ga('send','pageview');
        </script>
    </body>
</html>
