var send = 0;

$('.sendForm').click(function() {
     $('#contact-form').submit();
});

$('#contact-form').submit(function(e) {
     $('#error-msg').hide();
     $('#contact-form input').removeClass('error-input');
     var errorMsg = new Array();
     var i = 0;
     var error = 0;
     var email = $('#email').val();
     if (!validateemail(email)) {
          $('#email').addClass('error-input');
          error = 1;
          i++;
          errorMsg[i] = 'adres email';
     }

     var userName = $('#name').val();
     if (userName.length < 3) {
          $('#name').addClass('error-input');
          error = 1;
          i++;
          errorMsg[i] = 'imię i nazwisko';
     }

     var comment = $('#message').val();
     if (comment.length < 5) {
          $('#message').addClass('error-input');
          error = 1;
          i++;
          errorMsg[i] = 'komentarz jest za krótki';
     }

     if (error == 1) {
          $('#error-msg').html('Niepoprawnie wypełniony formularz, prosimy o uzupełnienie danych:<ul>');
          for (var j = 1; j <= i; j++) {
               var html = $('#error-msg').html();
               $('#error-msg').html(html + '' + '<li>' + errorMsg[j] + '</li>');
          }
          var html = $('#error-msg').html();
          $('#error-msg').html(html + '</ul>');
          $('#error-msg').show();
          return false;
     } else {
          if (send == 0) {
               send = 1;
               return true;
          }else {
               e.preventDefault();
               return false;
          }
     }
});

function validateemail(email) {
     var reg = /^([A-Za-z0-9_\-\.\+])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
     if (reg.test(email) == false) {
          return false;
     }
     else
     {
          return true;
     }
}

