function setNavigation() {
    var path = window.location.pathname;
    path = path.replace(/\/$/, "");
    path = decodeURIComponent(path);

//		console.log(path.substring(path.lastIndexOf("/")));

    $(".nav a").each(function () {
        var href = $(this).attr('href');
//        console.log(path.substring(path.lastIndexOf("/")+1)+" "+href);
        if (path.substring(path.lastIndexOf("/")+1) === href) {
            $(this).parent('li').addClass('active');
        }
    });
}


$(document).ready(function(){
	
  $('#fixedmenulink').bigSlide();
	
  setNavigation();

	if ($("#header-names").length>0) {

		$("#header-names").stick_in_parent({
				parent: $("#pricelist") 
		});
		
	}
	
	if ($(".tooltip").length>0) {
	
	  $('.tooltip').tooltipster({
		  maxWidth: "300",
		  delay: 50
	  });

	}

	if ($(".tooltip").length>0) {

	  $('.opcja-tooltip').tooltipster({
		  position: "bottom",
		  delay: 50
	  });

	}

  if ($('#laptop a').length>0) {
    
	  $('#laptop a').magnificPopup({
		  type: 'image',
		  gallery:{
		    enabled:true
		  },
	    zoom: {
				enabled: true
			}
		});

	}

	var prices = [
		
		[ [9,99],[19,99],[29,99], "5 stron", "5 stron" ], // 1 miesiąc
		[ [9,49],[18,99],[28,49], "20 stron", "20 stron"], // 3 miesiące
		[ [8,79],[17,59],[26,39], "100 stron", "100 stron" ], // 12 miesięcy
		[ [8,79],[17,59],[26,39], "250 stron", "250 stron" ]  // 24 miesięcy
		
	]

	$(".options").each(function(){
		
		$(this).find(".option").each(function(i,e){
		
		$(this).click(function(){
			
			$(".options .option").removeClass("active");
			$(".options .option:nth-of-type("+(i+1)+")").addClass("active");
						
			$("#header-prices li").each(function(i2,e2){
				
				$(this).find(".zlote").text(prices[i][i2][0]);
				$(this).find(".grosze").text(prices[i][i2][1]);
				
			})
			
			$(".package-header").each(function(i2,e2){
				
				$(this).find(".zlote").text(prices[i][i2][0]);
				$(this).find(".grosze").text(prices[i][i2][1]);
				
			})
			
			
//			$(".ocr-podstawowy").text(prices[i][3]);
//			$(".ocr-pelny").text(prices[i][4]);			
			
			if (i>2) {
				$(".trzy-miesiace-gratis").show();						
			} else {
				$(".trzy-miesiace-gratis").hide();			
			}
	
			})
				
		})
				
	})
	
	$(".options .option:last-child").trigger("click");
	
	
	$('#package-list li').click(function(){
	
		var tab_id = $(this).attr('data-tab');

		$('#package-list li').removeClass('aktywny');
		$('#package-details .package').removeClass('aktywny');

		$(this).addClass('aktywny');
		$("#"+tab_id).addClass('aktywny');
		console.log("1");
		$(document.body).trigger("sticky_kit:recalc");
	
	})

	$('#package-list li:nth-child(2)').trigger("click");

	
	$(".sticky").stick_in_parent({
			parent: $("#package-details") 
	});
    
    $('#acordion li').click(function(){
        console.log("test1");
        $(document.body).trigger("sticky_kit:recalc");
    });
    
	
	$("#package-details .more").each(function(){
		
		$(this).click(function(){
			var more = $(this);
			$(this).parent().find(".details").slideToggle(
				function(){ 
                    console.log("2");
					$(document.body).trigger("sticky_kit:recalc");
					more.hide();
				});
		
		})
		
	})

	$("#package-details .details .close").each(function(){
		$(this).click(function(){
			$(this).parent().slideUp(function(){			
				$(this).parent().find(".more").show();
                console.log("3");
				$(document.body).trigger("sticky_kit:recalc");
			})
		})
	});


$(function() {
  $('a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
  });
});


})