<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>O systemie do wystawiania faktur | szybkafaktura.pl</title>
        <meta name="keywords" content="faktura online, fakturowanie online, faktura vat, faktura vat online" />
<meta name="description" content="Twórcą szybkafaktura.pl jest spółka Cloud Planet S.A. specjalizująca się w tworzeniu rozwiązań informatycznych dla mikro i małych firm." />
        <meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="canonical" href="http://szybkafaktura.pl/o-nas.php" />
				<?php include_once("includes/head.php") ?>
    </head>
    <body class="product-page" id="o-nas">

				<?php include_once("includes/header.php") ?>

				<section id="intro">
					
					<div class="container">
						<div class="inside">
						
							<h1>O nas</h1>
							
							<p class="bigger">Twórcą szybkafaktura.pl jest spółka Cloud Planet S.A. specjalizująca się w tworzeniu rozwiązań informatycznych dla mikro i małych firm.</p>
							
							<p>System powstał z myślą o potrzebach przedsiębiorców związanych z prowadzeniem działalności gospodarczej i umożliwia tworzenie dokumentów księgowych niezbędnych do prowadzenia Księgi Przychodów i Rozchodów. szybkafaktura.pl wprowadza także nowe standardy w zakresie obsługi księgowej rekomendując usługi sprawdzonych biur rachunkowych z całej Polski.</p>

						</div>
					</div>
					
				</section>

				<section id="history">
					
					<div class="container">
						<div class="inside">
		
							<div class="event">
								<div class="year">2009</div>
								<p>Uruchomienie portalu szybkafaktura.pl jako jednego z pierwszych polskich serwisów do wystawiania faktur online</p>
							</div>
							
							<div class="event">
								<div class="year">2010</div>						
								<p>Wprowadzenie możliwości samodzielnego prowadzenia KPiR oraz modułu zarządzania magazynem. Spółka otrzymuje dofinansowanie w ramach dotacji UE (ok. 2,0 mln PLN) oraz nagrodę "Produkt Roku 2010" w plebiscycie magazynu PC WORLD</p>
							</div>
		
		
							<div class="event">
								<div class="year">2011</div>
								<p>Uruchomienie platformy B2B dla klientów i biur rachunkowych w ramach tworzenia Ogólnopolskiego Internetowego Biura Rachunkowego. Spółka nawiązuje również współpracę z bankami w celu uatrakcyjnienia oferty konta bankowego dla sektora MŚP</p>
							</div>
		
							<div class="event">
								<div class="year">2012</div>
								<p>Spółka pozyskała dodatkowy kapitał na dalszy rozwój od nowego inwestora - grupy inwestycyjnej MCI Management SA. Na koniec grudnia zostanie zaprezentowana szybkafaktura.pl w nowej odsłonie!</p>
							</div>							
		
							<div class="event">
								<div class="year">2013</div>
								<p>Rozpoczęcie projektu z największym polskim bankiem, PKO BP S.A. Od maja 2013 r. klienci biznesowi Banku otrzymują bezpłatny dostęp do systemu szybkafaktura.pl oraz możliwość obsługi przez rekomendowane biura rachunkowe współpracujące z Cloud Planet S.A.</p>
							</div>

							<div class="event">
								<div class="year">2014</div>
								<p>Fuzja spółek Cloud Planet S.A. dostawcy usług księgowości online szybkafaktura.pl, oraz Skanuj.to Sp. z o.o. innowacyjnego dostawcy rozwiązań automatyzujących rozpoznawanie danych z dokumentów księgowych. To połączenie wzbogaci o innowacyjne rozwiązania produkty spółki Cloud Planet S.A.</p>
							</div>

							<div class="event">
								<div class="year">2015</div>
								<p>Zakończenie integracji systemu szybkafaktura.pl oraz skanuj.to. Zmiany i usprawnienia dotyczyły całego systemu. Największą wdrożoną innowacją jest moduł rozpoznawania danych z dokumentów oraz faktur na podstawie skanu lub zdjęcia. Jako pierwsi na rynku wprowadzamy system OCR dostępny dla każdego użytkownika biznesowego korzystającego z modułu fakturowania.

</p>
							</div>


						</div>
					</div>
				
				</section>
				
				<section id="news">
					
					<div class="container">
						
						<div class="inside">
							
							<h2>Aktualności</h2>
							
							<div class="news-item">
								
								<h3>Cloud Planet S.A. łączy się z Skanuj.to Sp. z o.o.</h3>
								<p class="date">13.06.2014</p>
								<p class="teaser">15 maja 2014r. doszło do połączenia spółek Cloud Planet S.A. („Cloud Planet”), dostawcy usług księgowości online szybkafaktura.pl, oraz Skanuj.to Sp. z o.o. („Skanuj.to”), innowacyjnego dostawcy rozwiązań automatyzujących rozpoznawanie danych z dokumentów księgowych.</p>

							</div>

							<div class="news-item">
								
								<h3>Współpracuj z najlepszymi – możliwość wyboru biura rachunkowego</h3>
								<p class="date">19.06.2013r</p>
								<p class="teaser">Od początku czerwca 2013r. na stronie głównej szybkafaktura.pl wprowadzona została możliwość wyboru biura rachunkowego. Budowana w ramach projektu „Księgowość online z PKO BP” sieć partnerska z biurami rachunkowymi w całym kraju pozwala nam rekomendować użytkownikom szybkafaktura.pl usługi księgowe na najwyższym poziomie. W ramach projektu z bankiem PKO BP zachęcamy również naszych użytkowników do zapoznania się atrakcyjną ofertą rachunków biznesowych dla małych i średnich przedsiębiorstw.</p>
								
							</div>
							
							<div class="news-item">
								
								<h3>Cloud Planet S.A. rozpoczyna współpracę z PKO BP S.A.</h3>
								<p class="date">02.05.2013r</p>
								<p class="teaser">Z przyjemnością zawiadamiamy, że Cloud Planet S.A. nawiązała współpracę z największym polskim bankiem – PKO BP. Wraz z początkiem maja br., każdy klient banku PKO BP, który wybierze jeden z pakietów dla przedsiębiorców: Biznes Debiut 18, Biznes Rozwój, Biznes Komfort Plus lub Biznes Sukces Plus otrzyma bezpłatny 6-miesięczny dostęp do platformy szybkafaktura.pl. Dziękujemy za zaufanie i zapraszamy do zapoznania się z nową ofertą PKO BP dla przedsiębiorców. </p>
								
							</div>
							
						</div>
						
					</div>
					
				</section>
				
				<section id="quotes">
					
					<div class="container">
						<div class="inside">
						
							<ul class="quotes cycle-slideshow" data-cycle-timeout="2000" data-cycle-slides="> li.quote">
								
								<li class="quote">
									<span class="quote-content">Odkąd pracuję w systemie szybkafaktura.pl, nasi klienci doceniają zalety współpracy online, zwłaszcza prosty moduł do fakturowania i automatyczne przekazywanie dokumentów do biura.</span>
									<span class="quote-author">Paweł Pec, Doradca Podatkowy, numer 05502 Biuro Rachunkowo-Konsultingowe MAX, Poznań</span>
								</li>

						    <div class="cycle-pager"></div>		

							</ul>
						
						</div>
					</div>					
					
				</section>

				<?php include_once("includes/footer.php") ?>

        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='//www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-1326744-3');
			ga('set', 'contentGroup1', 'Grupa www kod'); 
			ga('send','pageview');
        </script>
    </body>
</html>
