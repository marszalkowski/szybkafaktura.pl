<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="canonical" href="http://szybkafaktura.pl/program-partnerski.php" />
				<?php include_once("includes/head.php") ?>
    </head>
    <body class="product-page" id="program-partnerski">

				<?php include_once("includes/header.php") ?>

				<section id="intro" style="min-height: 600px;">
					
					<div class="container">
						<div class="inside">
						
							<h1>Program partnerski</h1>
							
							<p>Prosimy o kontakt wszystkich, którzy są zainteresowani współpracą<br>
								oraz sprzedażą rozwiazań na adres <a href="mailto:partner@cloud-planet.pl">partner@cloud-planet.pl</a></p>
								
							<div class="image-container" id="rece" style="margin: 100px 0">
								<img src="img/program.svg" alt="" style="width: 400px" />
								

						</div>
					</div>
					
				</section>
				
				<section id="quotes">
					
					<div class="container">
						<div class="inside">
						
							<ul class="quotes cycle-slideshow" data-cycle-timeout="2000" data-cycle-slides="> li.quote">
								
								<li class="quote">
									<span class="quote-content">Odkąd pracuję w systemie szybkafaktura.pl, nasi klienci doceniają zalety współpracy online, zwłaszcza prosty moduł do fakturowania i automatyczne przekazywanie dokumentów do biura.</span>
									<span class="quote-author">Paweł Pec, Doradca Podatkowy, numer 05502 Biuro Rachunkowo-Konsultingowe MAX, Poznań</span>
								</li>

						    <div class="cycle-pager"></div>		

							</ul>
						
						</div>
					</div>					
					
				</section>

				<?php include_once("includes/footer.php") ?>

        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='//www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-1326744-3');
			ga('set', 'contentGroup1', 'Grupa www kod'); 
			ga('send','pageview');
        </script>
    </body>
</html>
