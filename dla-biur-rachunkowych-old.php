<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Dla biur rachunkowych | szybkafaktura.pl</title>
        <meta name="keywords" content="faktura online, fakturowanie online, faktura vat, faktura vat online" />
<meta name="description" content="Ułatwiamy przedsiębiorcom znalezienie profesjonalnych usług księgowych w swoim mieście lub regionie." />
        <meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="canonical" href="http://szybkafaktura.pl/dla-biur-rachunkowych.php" />
				<?php include_once("includes/head.php") ?>
        
    </head>
    <body id="dla-biur">

				<?php include_once("includes/header.php") ?>

    <!-- <section id="main">
	    
	    <div class="bg"><div class="circles"></div></div>

	    <div class="container">

			<div class="lead"><div class="inside">
			
				<h1>Stań się widoczny<small>Dołącz do projektu!</small></h1>
				<p class="big">Ułatwiamy przedsiębiorcom znalezienie<br>
				profesjonalnych usług księgowych<br>
				w swoim mieście lub regionie.</p>
				
				<p class="small">ps. Nasz serwis to także wiele<br>
				usług dodatkowych oferowanych<br>
				przez naszych Partnerów.</p>
				
				<div class="button-container" style="margin-top: 20px">
					<a class="big green button newButtonsignup" href="#package-list">Pokaż pakiety</a>
				</div>
				
			</div></div>
			
			<div class="feature" id="nowoczesne-narzedzia">
			
				<figure><img src="img/dla-biur-rachunkowych/feature-1.png"  alt="Oprogramowanie dla biur rachunkowych" /></figure>
				<h3><span>Nowoczesne narzędzia</span><br>
				<span>dla Ciebie i Twoich klientów</span></h3>

			</div>

			<div class="feature" id="promocja-w-internecie">
			
				<figure><img src="img/dla-biur-rachunkowych/feature-2.png"  alt="Promocja w internecie dla biur rachunkowych" /></figure>
				<h3><span>Promocja w internecie</span></h3>
				
			</div>

			<div class="feature" id="przewaga-konkurencyjna">
			
				<figure><img src="img/dla-biur-rachunkowych/feature-3.png"  alt="przewaga-konkurencyjna w internecie dla biur rachunkowych" /></figure>
				<h3><span>Przewaga</span><br />
				<span>konkurencyjna</span><br/>
				<span>na rynku</span></h3>
				
			</div>

			<div class="feature" id="uczestnictwo-w-projektach">
			
				<figure><img src="img/dla-biur-rachunkowych/feature-4.png"  alt="internecie dla biur rachunkowych" /></figure>
				<h3><span>Możliwość uczestnictwa</span><br>
				<span>w wielu innych projektach</span></h3>
				
			</div>

			<div class="feature" id="partnerstwo-z-bankami">
			
				<figure><img src="img/dla-biur-rachunkowych/feature-5.png"  alt="partnerstwo z bankam dla biur rachunkowych" /></figure>
				<h3><span>Partnerstwo</span><br>
				<span>z największymi</span><br>
				<span>bankami w Polsce</span></h3>
				
			</div>


    </div></section> -->


    <section id="package-list">
	    <div class="container">

				<h3>Wybierz pakiet i dołącz do grona sprawdzonych księgowych!</h3><br/><br/>

				<ul>
					<li class="podstawowy" data-tab="podstawowy"><label>Podstawowy</label></li>
					<li class="premium" data-tab="premium"><label>Premium</label></li>
					<!-- <li class="bankowy" data-tab="bankowy"><label>Bankowy Księgowy<label></li> -->
				</ul>

	    </div>
    </section>
    
    <div id="package-details">

    <section id="podstawowy" class="package">
	    
	    <div class="intro">
		    
		    <div class="container">
			    
					<div class="sticky">
						
						<div class="price">
							<span class="price-label">cena pakietu</span>
							<span class="price-value">0 zł</span>
						</div>
						
						<span class="button-container">
							<a href="http://sprawdzonyksiegowy.pl/rejestracja" class="small darkblue button newButtonLogin">Załóż wizytówkę</a>
						</span>
						
					</div>
					
					<div class="intro-text">
						
						<h2>Pakiet Podstawowy</h2>
						<p>Dołącz do grona sprawdzonych księgowych!<br>
							 Zacznij od rejestracji wizytówki firmy, bezpłatnie<br>
							 i bez żadnych zobowiązań. </p>
						
					</div>
			    
		    </div>
		    
	    </div>
    
    	<div class="container">
    	
    		<h3>Dlaczego warto?</h3>
    		
    		<ul class="features">	    		
	    		<li>Zwiększysz widoczność  swojej firmy w internecie</li>
	    		<li>Otrzymasz możliwość dotarcia do klientów poszukujących usług księgowych w Twoim regionie</li>
	    		<li>Zaoszczędzisz na wydatkach związanych z reklamą firmy</li>
    		</ul>
								
				<h3>Jak założyć bezpłatną wizytówkę?</h3>
				<p>Wypełnij <a href="http://sprawdzonyksiegowy.pl/rejestracja">krótki formularz rejestracyjny</a>, a już po chwili dane Twojej firmy<br>
					 będą widoczne w serwisie sprawdzonyksiegowy.pl</p>
			
			</div>
		
		</section>    
    <section id="premium" class="package">
	    
	    <div class="intro">
		    
		    <div class="container">
			    
					<div class="sticky">
						
						<div class="price">
							<span class="price-label">cena pakietu</span>
							<span class="price-value">50 zł</span>
							<span class="price-monthly">za miesiąc</span>
						</div>
						
						<span class="button-container">
							<a rel="modal:open" href="wyslij-zgloszenie.php?pacakge=premium" class="small darkblue button newButtonLogin">Wyślij zgłoszenie</a>
						</span>
						
					</div>
					
					<div class="intro-text">
						
						<h2>Pakiet Premium</h2>
						<p>Pakiet PREMIUM to odpowiedź na potrzeby biur rachunkowych, które chcą wyróżnić wizytówkę swojej firmy i zyskać przewagę konkurencyjną.</p>
						
					</div>
			    
		    </div>
		    
	    </div>
    
    	<div class="container">
    	
    		<h3>Dlaczego warto?</h3>
				
				<ul class="features">
					
					<li>Samodzielnie zarządzasz treścią wizytówki</li>
					<li>Monitorujesz na bieżąco statystyki odwiedzin</li>
					<li>Otrzymasz kontakty do osób zainteresowanych ofertą firmy</li>
					<li>Publikujesz opinie klientów</li>
					<li>Promujemy Twoje usługi w ramach KSIĘGOWOŚĆ all inclusive
					
					<div class="details all-inclusive">
						
						<h4><strong>KSIĘGOWOŚĆ all inclusive</strong>to usługa dedykowana firmom sektora MŚP.</h4>
						<p>Przedsiębiorca otrzymuje w jednej cenie:</p>
						<ol>
								<li>prosty program do fakturowania z możliwością komunikacji z księgowym</li>
								<li>obsługę księgową świadczoną przez certyfikowane biuro rachunkowe</li>
						</ol>
						<h4>Jak to działa?</h4>
						
						<figure>
						
							<div class="przedsiebiorca">
								Przedsiębiorca wystawia faktury,<br>
								dodaje skany faktur kosztowych lub<br>
								innych dokumentów	księgowych.</div>
							<div class="biuro">
								Biuro rachunkowe sprawdza poprawność<br>
								wprowadzonych danych, księguje<br>
								dokumenty, dokonuje rozliczeń,<br>
								oblicza podatki, wysyła deklaracje.</div>
						
						
						</figure>
						
						<h4>Zalety rozwiązania</h4>
						<ul class="advantages">
							<li>proste i wygodne narzędzie do komunikacji z klientem</li>
							<li>wsparcie marketingowe i promocja usług Twojego biura rachunkowego</li>
							<li>możliwość pozyskania nowych klientów do obsługi księgowej</li>
							<li>wygoda i pewność otrzymania zapłaty za wykonane usługi - dbamy o terminowe uiszczanie płatności Twoich klientów i przekazujemy Ci wynagrodzenie za wykonaną usługę</li>
						</ul>

						<span class="close">Zamknij opis</span>
						
					</div>
					
					<br><span class="more">Dowiedz się więcej</span></li>
					<li>Udostępniamy dla Ciebie i Twoich klientów nowoczesny system Xenga
					
					<div class="details xenga">
						
						<h4><strong>Xenga</strong> 
							  powstała z myślą o usprawnieniu pracy biur rachunkowych</h4>
						<ul class="advantages">
							<li>Zapewnia Twoim klientom proste i wygodne narzędzie do fakturowania oraz monitorowania finansów</li>
							<li>Umożliwia wymianę danych i komunikację z klientem</li>
							<li>Odczytuje skanowane dokumenty i importuje dane do programów księgowych</li>
						</ul>

						<span class="close">Zamknij opis</span>
						
					</div>
					
					<br><span class="more">Dowiedz się więcej</span> </li>
					<!-- <li>Otrzymujesz dodatkowe wyróżnienie wizytówki
						<div class="icons">
							<img src="img/dla-biur-rachunkowych/all-inclusive-active.png" alt="Księgowość All Inclusive" />
							<img src="img/dla-biur-rachunkowych/xenga-active.png" alt="Xenga" />
						</div> -->					
					</li>
					
				</ul>
				
				<h3>Jesteś zainteresowany współpracą?</h3>
				<p>Skontaktuj się z naszym Biurem Obsługi Klienta<br>
					pod numerem 58 732 79 36 lub <a rel="modal:open" href="wyslij-zgloszenie.php?pacakge=premium">Wyślij zgłoszenie</a></p>

			
			</div>
		
		</section>
    <section id="bankowy" class="package">
	    
	    <div class="intro">
		    
		    <div class="container">
			    
					<div class="sticky">
						
						<div class="price">
							<span class="price-label">cena pakietu</span>
							<span class="price-value"><span class="od">od</span>150 zł</span>
							<span class="price-monthly">za miesiąc</span>
						</div>
						
						<span class="button-container">
							<a rel="modal:open" href="wyslij-zgloszenie.php?pacakge=bankowy-ksiegowy" class="small darkblue button newButtonLogin">Wyślij zgłoszenie</a>
						</span>
						
					</div>
					
					<!-- <div class="intro-text">
						
						<h2>Pakiet Bankowy Księgowy</h2>
						<p>Pakiet <strong>PREMIUM</strong> to za mało? Zostań <strong>BANKOWYM KSIĘGOWYM!</strong></p>
						
					</div> -->
			    
		    </div>
		    
	    </div>
    
    	<!-- <div class="container">
    	
    		<h3>W ramach pakietu BANKOWY KSIĘGOWY otrzymujesz</h3>
    		
				<ul class="features">
					
					<li class="pakiet-premium">Pakiet PREMIUM</li>
					<li class="plus">plus</li>
					<li>Możliwość współpracy z dowolną ilością wybranych przez siebie oddziałów banku PKO BP (obowiązuje opłata 100 zł za każdy oddział)</li>
					<li>Rekomendację swoich usług księgowych klientom banku PKO BP</li>
					<li>Szansę prowadzenia obsługi księgowej klientom banku</li>
					<li>Certyfikat Rzetelności potwierdzający najwyższą jakość usług</li>
					<li>Dodatkowe wyróżnienie wizytówki
						<div class="icons">
							<img src="img/dla-biur-rachunkowych/bankowy-ksiegowy-active.png" alt="Bankowy Księgowy" />
							<img src="img/dla-biur-rachunkowych/pko-a.png" alt="PKOBP" />
						</div>					
					</li>

				</ul>
												
				<h3>Jak wygląda proces przystąpienia do projektu BANKOWY KSIĘGOWY?</h3>
				
				<ul class="process">
					<li>Skontaktuj się z naszym Biurem Obsługi Klienta pod numerem 58 732 79 36 lub <a rel="modal:open" href="/wyslij-zgloszenie/bankowy-ksiegowy">Wyślij zgłoszenie</a></li>
					<li>Otrzymasz ankietę zgłoszeniową oraz umowę o współpracy.</li>
					<li>Gotowe! Korzystasz ze wszystkich benefitów projektu BANKOWY KSIĘGOWY.</li>
				</ul>

			</div>
		 -->
		</section> 	
		
    </div>
		
				<?php include_once("includes/footer.php") ?>

        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='//www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-1326744-3');
			ga('set', 'contentGroup1', 'Grupa www kod'); 
			ga('send','pageview');
        </script>
    </body>
</html>
