<?php
$sessionId = session_id();

if(empty($sessionId)) {
     @session_start();
}

$providerGet = null;
if (!empty($_GET['provider'])) {
     $providerGet = $_GET['provider'];
     $_SESSION['provider'] = $providerGet;
} else if (!empty($_SESSION['provider'])) {
     $providerGet = $_SESSION['provider'];
}
?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>Program do wystawiania faktur online na SzybkaFaktura.pl</title>
        <meta name="keywords" content="faktura online, fakturowanie online, faktura vat, faktura vat online" />
<meta name="description" content="Internetowy program do wystawiana faktur. Faktury online. Księgowość przez internet. Niskie koszty obsługi. Doświadczenie w wystawianiu faktur. Oszczędność czasu. Zapraszamy do kontaktu." />
<link rel="canonical" href="http://szybkafaktura.pl/" />
				<?php include_once("includes/head.php") ?>
				<script type="text/javascript" src="//userlike-cdn-widgets.s3-eu-west-1.amazonaws.com/fd3c7d5ef3456e6c750cbdeb1d0003bef0af3e54edba59fc04ab6b62127796af.js"></script> 
                                <script src="https://track.omgpl.com/action/application/?MID=456446&PID=10625&val=&action=Home"></script>
								
<!-- Start Visual Website Optimizer Synchronous Code -->
<script type='text/javascript'>
var _vis_opt_account_id = 90510;
var _vis_opt_protocol = (('https:' == document.location.protocol) ? 'https://' : 'http://');
document.write('<s' + 'cript src="' + _vis_opt_protocol +
'dev.visualwebsiteoptimizer.com/deploy/js_visitor_settings.php?v=1&a='+_vis_opt_account_id+'&url='
+encodeURIComponent(document.URL)+'&random='+Math.random()+'" type="text/javascript">' + '<\/s' + 'cript>');
</script>

<script type='text/javascript'>
if(typeof(_vis_opt_settings_loaded) == "boolean") { document.write('<s' + 'cript src="' + _vis_opt_protocol +
'd5phz18u4wuww.cloudfront.net/vis_opt.js" type="text/javascript">' + '<\/s' + 'cript>'); }
// if your site already has jQuery 1.4.2, replace vis_opt.js with vis_opt_no_jquery.js above
</script>

<script type='text/javascript'>
if(typeof(_vis_opt_settings_loaded) == "boolean" && typeof(_vis_opt_top_initialize) == "function") {
        _vis_opt_top_initialize(); vwo_$(document).ready(function() { _vis_opt_bottom_initialize(); });
}
</script>
<!-- End Visual Website Optimizer Synchronous Code -->
    </head>
    <body class="home">

				<?php include_once("includes/header.php") ?>

				<section id="main">
					
					<div class="container">
						<div class="inside">
							
							<div class="lead">

								<h1>Program do wystawiania<br>
									faktur online <br class="hide show-on-phone">i nie tylko</h1>
									
								<p>Skorzystaj z intuicyjnego oprogramowania<br class="hide-on-phone">
								do wystawiania faktur, obsługuj magazyn,<br class="hide-on-phone">
								prowadź księgowość przez internet <br class="hide-on-phone">
								lub wybierz sprawdzone <br class="hide-on-phone">
								biuro księgowe.</p>
								<a href="https://app.szybkafaktura.pl/auth/new-register?app=skto<?php echo ($providerGet !== null) ? '&provider='.$providerGet : '' ?>" class="big green button button-45-days">Wypróbuj za darmo
								</a><br/><img src="/img/jpk_vat.png">
								
								
							</div>

						</div>
					</div>
					
				</section>				

				<?php include_once("includes/footer.php") ?>

        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='//www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-1326744-3');ga('send','pageview');
        </script>
    </body>
</html>
