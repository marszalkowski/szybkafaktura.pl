<?php
$sessionId = session_id();

if(empty($sessionId)) {
     @session_start();
}

$providerGet = null;
if (!empty($_GET['provider'])) {
     $providerGet = $_GET['provider'];
     $_SESSION['provider'] = $providerGet;
} else if (!empty($_SESSION['provider'])) {
     $providerGet = $_SESSION['provider'];
}
?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Księgowość online to idealne rozwiązanie dla przedsiębiorców | szybkafaktura.pl</title>
        <meta name="keywords" content="faktura online, fakturowanie online, faktura vat, faktura vat online" />
<meta name="description" content="Księgowość online to idealne rozwiązanie dla przedsiębiorców
								 rozliczających się na podstawie Podatkowej Księgi Przychodów i Rozchodów." />
        <meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="canonical" href="http://szybkafaktura.pl/ksiegowosc-online.php" />
				<?php include_once("includes/head.php") ?>

    </head>
    <body class="product-page" id="zarzadzaj-magazynem">

				<?php include_once("includes/header.php") ?>

				<section id="intro">
					
					<div class="container">
						<div class="inside">
						
							<h1>Księgowość internetowa, rozliczaj samodzielnie</h1>
							
							<p>Księgowość online to idealne rozwiązanie dla przedsiębiorców<br class="hide-on-phone">
								 rozliczających się na podstawie Podatkowej Księgi Przychodów i Rozchodów. </p>
							<p>Jest to wygodny i prosty w obsłudze program, idealnie nadający się<br class="hide-on-phone">
								 do zarządzania mikro lub małym przedsiębiorstwem.</p>

							
							<div id="laptop">
								
								<div id="desktop">

									<a id="front-screen" href="screens/ksiegowosc-online/9.png">
										<img src="screens/ksiegowosc-online/9.png" alt="Rozliczanie samodzielnie księgowości" />
										<span class="zoom1">&nbsp;</span>
										<span class="zoom2">&nbsp;</span>
									</a>
									<a id="second-screen" href="screens/ksiegowosc-online/12.png">
										<img src="screens/ksiegowosc-online/12.png" alt="Rozliczanie samodzielnie księgowości" />
										<span class="zoom1">&nbsp;</span>
										<span class="zoom2">&nbsp;</span>
									</a>
									
								</div>
								
							</div>

						
						</div>
					</div>
					
				</section>
				
				<section id="features">
					
					<div class="container">
						<div class="inside">
						
							<div class="feature" id="latwosc-obslugi">
								
								<figure><img src="img/image-7.png" alt="Program do KPiR"></figure>

								<div class="text">
																
									<h2>Profesjonalny program do&nbsp;KPiR</h2>
									<p>System jest w pełni dostosowany do potrzeb przedsiębiorców rozliczających się na podstawie KPiR i zawsze aktualny z obowiązującymi przepisami. </p>
								
								</div>
						
							</div>

							<div class="feature" id="automatyczne-przetwarzanie">
																
								<figure class="show-on-phone"><img src="img/image-8.png" alt="Program do KPiR"></figure>

								<div class="text">
									
									<h2>Oszczędność czasu</h2>
									<p>Przejrzysty interfejs sprawia, że tworzenie dokumentów, KPiR, deklaracji podatkowych jest niezwykle intuicyjne i zajmuje zaledwie kilka chwil. Dzięki funkcji automatycznego rozpoznawania danych z zeskanowanych dokumentów (OCR), wprowadzanie kosztów zajmuje tylko kilka chwil.</p>
								
								</div>

								<figure class="hide-on-phone"><img src="img/image-8.png" alt="Program do KPiR"></figure>
						
							</div>

							<div class="feature" id="kontrola-finansow">
																
								<figure><img src="img/image-9.png" "Program do OCR"></figure>

								<div class="text">

									<h2>Mniejsze koszty</h2>
									<p>Dzięki szybkafaktura.pl możesz optymalizować wydatki w swojej firmie. Od tej pory koszt prowadzenia księgowości będzie niższy nawet o kilkaset złotych miesięcznie.</p>

								</div>
						
							</div>

						
						</div>
					</div>
					
				</section>
				
				<section id="signup">
					
					<div class="container">
						<div class="inside">
							<p>Dołącz do grona zadowolonych klientów.</p><a href="https://app.szybkafaktura.pl/auth/new-register?app=skto<?php echo ($providerGet !== null) ? '&provider='.$providerGet : '' ?>" class="medium button green signup"><span></span><label>Załóż konto</label></a><p>Wypróbuj przez 45 dni za darmo!</p>
						</div>
					</div>
					
				</section>
				
				<section id="details">
					
					<div class="container">
						<div class="inside">
						
							<h2>Najważniejsze funkcje</h2>
							
							<div class="row">
							
							<ul class="column">
								<li>wystawianie dokumentów sprzedaży (faktury)</li>
								<li>wprowadzanie i digitalizacja dokumentów kosztowych</li>
								<li>dodawanie pracowników, umów i wypłat</li>
								<li>lista płac i karta pracownika z możliwością drukowania i wysyłania</li>
								<li>delegacje</li>
								<li>kartoteka pojazdów</li>
								<li>kilometrówka</li>
								<li>kartoteka środków trwałych</li>
								<li>amortyzacja środków trwałych, historia amortyzacji</li>
							</ul>
							
							<ul class="column">
								<li>magazyn, remanenty, automatyczna kontrola stanów magazynowych</li>
								<li>edytowalne opisy zdarzeń gospodarczych</li>
								<li>ewidencja VAT sprzedaży i zakupu – miesięcznie lub kwartalnie z możliwością drukowania i wysyłania (pdf)</li>
								<li>deklaracje podatkowe – VAT-7, VAT-UE, PIT-5, PIT-4R, PIT-11,</li>
								<li>deklaracje ZUS – DRA, RCA, ZUA, ZZA, ZWUA z eksportem do programu Płatnik</li>
								<li>deklaracje elektroniczne (e-deklaracje)</li>
								<li>JPK_VAT, JPK_KPiR</li>
								<li>obsługa ryczałtu - JPK_EWP i ewidencja przychodu</li>
							</ul>
							
							</div>
						
						</div>
					</div>					
					
				</section>

				<section id="quotes">
					
					<div class="container">
						<div class="inside">
						
							<ul class="quotes cycle-slideshow" data-cycle-timeout="2000" data-cycle-slides="> li.quote">
								
								<li class="quote">
									<span class="quote-content">Od kilku lat prowadzę firmę i od samego początku prowadzę swoją księgowość internetową w szybkafaktura.pl.<br>Prowadzenie księgowości jest naprawdę proste a przy tym pozwala znacząco zmniejszyć koszty.</span>
									<span class="quote-author">Magda, właściciel szkoły językowej</span>
								</li>


						    <div class="cycle-pager"></div>		

							</ul>
						
						</div>
					</div>					
					
				</section>

				<?php include_once("includes/footer.php") ?>


        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='//www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-1326744-3');
			ga('set', 'contentGroup1', 'Grupa www kod'); 
			ga('send','pageview');
        </script>
    </body>
</html>
