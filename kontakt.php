<?php
$userName = $_POST['name'];
$email = $_POST['email'];
$message = $_POST['message'];
$category = $_POST['category'];

if (!empty($_POST['email']) && !empty($_POST['name']) && !empty($_POST['message']) && !empty($_POST['category']) && !empty($_POST['g-recaptcha-response'])) {

     $secretKey = '6LcnUREUAAAAAFTa5YQS4B2SemmgFNxVcLhrf_i9';
     $response = $_POST['g-recaptcha-response'];
     $remoteip = $_SERVER['REMOTE_ADDR'];

     $responseGet = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=" . $secretKey . "&response=" . $response . "&remoteip=" . $remoteip);
     $responseKeys = json_decode($responseGet, true);

     //var_dump($responseKeys);
     
     if (intval($responseKeys["success"]) == 1) {
          switch ($category) {
               case 'QUESTION':
                    $category = 'Pytanie';
                    break;
               case 'COMMENT_OR_REMARK':
                    $category = 'Komentarz lub uwaga';
                    break;
               case 'BUG_REPORT':
                    $category = 'Zgłoszenie błędu w systemie';
                    break;
               case 'FEATURE_REQUEST':
                    $category = 'Pomysł na nową funkcjonalność';
                    break;
               case 'ONLINE_ACCOUNTING':
                    $category = 'Współpraca z szybkafaktura.pl';
                    break;
               default:
                    $category = 'Inne';
                    break;
          }

          $to = 'bok@szybkafaktura.pl';
          $subject = 'Wiadomość ze strony szybkafaktura.pl od ' . $userName . ' (' . $category . ')';
          $message = 'Imię i nazwisko: ' . $userName . "\n" . "\n" . $message;
          $headers = 'From: ' . $email . "\r\n" .
                  'Reply-To: ' . $email . "\r\n" .
                  'X-Mailer: PHP/' . phpversion();

          mail($to, $subject, $message, $headers);

          $userName = $email = $message = $category = '';

          $sended = 1;
     } else {
          $sended = 2;
     }
} else if (!empty($_POST['message']) && empty($_POST['g-recaptcha-response'])) {
     $sended = 2;
}
?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
     <head>
          <meta charset="utf-8">
          <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
          <meta name="description" content="">
          <meta name="viewport" content="width=device-width, initial-scale=1">

          <?php include_once("includes/head.php") ?>
          <link rel="canonical" href="http://szybkafaktura.pl/kontakt.php" />
          <script src='https://www.google.com/recaptcha/api.js'></script>
     </head>
     <body id="contact">

          <?php include_once("includes/header.php") ?>

          <section id="formularz">

               <div class="container">
                    <div class="inside">

                         <h1>Skontaktuj się z&nbsp;nami</h1>

                         <p class="text-center show-on-phone">
                              Wypełnij poniższy formularz lub wyślij wiadomość e-mail na adres <a href="mailto:bok@szybkafaktura.pl">bok@szybkafaktura.pl</a>
                              Odpowiedzi na wysłane zgłoszenia udzielamy, najpóźniej w kolejnym dniu roboczym</p>

                         <div class="row">
                              <?php if (!empty($sended) && $sended == 1): ?><h2 style="margin-left: 142px">Dziękujemy za kontakt!</h2> <?php endif; ?>
                              <?php if (!empty($sended) && $sended == 2): ?><h2 style="margin-left: 142px">Problem z CAPTCHA!</h2> <?php endif; ?>

                              <form id="contact-form" method="POST">

                                   <div class="select field">
                                        <label>Temat</label>
                                        <select id="category" name="category">
                                             <option value="QUESTION" <?php echo (!empty($category) && $category == 'QUESTION') ? 'SELECTED' : ''; ?>>Pytanie</option>
                                             <option value="COMMENT_OR_REMARK" <?php echo (!empty($category) && $category == 'COMMENT_OR_REMARK') ? 'SELECTED' : ''; ?>>Komentarz lub uwaga</option>
                                             <option value="BUG_REPORT" <?php echo (!empty($category) && $category == 'BUG_REPORT') ? 'SELECTED' : ''; ?>>Zgłoszenie błędu w systemie</option>
                                             <option value="FEATURE_REQUEST" <?php echo (!empty($category) && $category == 'FEATURE_REQUEST') ? 'SELECTED' : ''; ?>>Pomysł na nową funkcjonalność</option>
                                             <option value="ONLINE_ACCOUNTING" <?php echo (!empty($category) && $category == 'ONLINE_ACCOUNTING') ? 'SELECTED' : ''; ?>>Współpraca z szybkafaktura.pl</option>
                                             <option value="OTHER" <?php echo (!empty($category) && $category == 'OTHER') ? 'SELECTED' : ''; ?>>Inne</option>
                                        </select>
                                   </div>

                                   <div class="input field">
                                        <label>Adres email</label>
                                        <input name="email" id="email" type="text" value="<?php echo!empty($email) ? $email : ''; ?>"/>
                                   </div>
                                   <div class="input field">
                                        <label>Imię i Nazwisko</label>
                                        <input id="name" name="name" type="text" value="<?php echo!empty($userName) ? $userName : ''; ?>"/>
                                   </div>

                                   <div class="textarea field">
                                        <label>Wiadomość</label>
                                        <textarea name="message" id="message"><?php echo!empty($message) ? $message : ''; ?></textarea>
                                   </div>
                                   <div class="error error-registry" id="error-msg"></div>
                                   <div class="g-recaptcha" data-sitekey="6LcnUREUAAAAAB-Foxsu-qrZwzMMXzZLahUg6TPY"></div>

                                   <button type="button" class="send blue medium button sendForm newButtonLogin">Wyślij formularz</button>

                              </form>

                              <div class="text">

                                   <ul>
                                        <li>Chcesz dowiedzieć się więcej o księgowości online?</li>
                                        <li>Chcesz rozpocząć współpracę z nami?</li>
                                        <li>Masz do nas pytanie lub chcesz się podzielić uwagami dotyczącymi funkcjonowania systemu szybkafaktura.pl?</li>
                                        <li>Zauważyłeś błąd w działaniu systemu?</li>
                                        <li>Masz pomysł na nową funkcjonalność?</li>
                                   </ul>

                                   <p>Wypełnij formularz lub wyślij wiadomość e-mail na adres <a href="mailto:bok@szybkafaktura.pl">bok@szybkafaktura.pl</a></p>
                                   <p>Odpowiedzi na wysłane zgłoszenia udzielamy, najpóźniej w kolejnym dniu roboczym</p>

                              </div>

                         </div>


                    </div>
               </div>

          </section>

          <section id="address">

               <div class="container">
                    <div class="inside">

                         <div class="row">

                              <div class="column">

                                   <h3>Biuro Obsługi Klienta</h3>
                                   <ul>
                                        <li class="telefon">+48 58 732 79 36</li>
                                        <li class="email"><a href="mailto:bok@szybkafaktura.pl">bok@szybkafaktura.pl</a></li>
                                   </ul>

                              </div>

                              <div class="column">

                                   <h3>Dla biur rachunkowych</h3>
                                   <ul>
                                        <li class="telefon">+48 58 732 18 97</li>
                                        <li class="email"><a href="mailto:bok@sprawdzonyksiegowy.pl">bok@sprawdzonyksiegowy.pl</a></li>
                                   </ul>

                              </div>

                              <div class="column">

                                   <h3>Marketing / Partnerzy</h3>
                                   <ul>
                                        <li class="telefon">+48 58 732 17 87</li>
                                        <li class="email"><a href="mailto:marketing@loud-planet.pl">marketing@cloud-planet.pl</a></li>
                                   </ul>

                              </div>

                         </div>

                    </div>
               </div>
          </section>

          <section id="map">

               <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d2323.700989113996!2d18.6073278!3d54.379932399999994!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46fd749571b7cf21%3A0x4f1dbaab523f47f5!2sDmowskiego+14%2C+80-264+Gda%C5%84sk!5e0!3m2!1spl!2spl!4v1421772294786" width="600" height="450" frameborder="0" style="border:0; width: 100%"></iframe>					
          </section>

          <section id="footnote">

               <div class="container">
                    <div class="inside">

                         <p class="small">Cloud Planet S.A., ul.Dmowskiego 14/4a
                              80-264 Gdańsk, wpisana do Rejestru Przedsiębiorców Krajowego Rejestru Sądowego prowadzonego przez Sąd Rejonowy Gdańsk-Północ w Gdańsku, Wydział VIII Gospodarczy Krajowego Rejestru Sądowego pod numerem KRS: 0000317657. Kapitał zakładowy: 833 334,00 PLN w pełni opłacony. NIP 2040002259, REGON 220683320</p>							

                    </div>
               </div>

          </section>

          <?php include_once("includes/footer.php") ?>
          <script src="js/contactForm.js"></script>

          <script>
               (function (b, o, i, l, e, r) {
                    b.GoogleAnalyticsObject = l;
                    b[l] || (b[l] =
                            function () {
                                 (b[l].q = b[l].q || []).push(arguments)
                            });
                    b[l].l = +new Date;
                    e = o.createElement(i);
                    r = o.getElementsByTagName(i)[0];
                    e.src = '//www.google-analytics.com/analytics.js';
                    r.parentNode.insertBefore(e, r)
               }(window, document, 'script', 'ga'));
               ga('create', 'UA-XXXXX-X');
               ga('send', 'pageview');
          </script>
     </body>
</html>
