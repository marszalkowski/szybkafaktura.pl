<?php
$sessionId = session_id();

if(empty($sessionId)) {
     @session_start();
}

$providerGet = null;
if (!empty($_GET['provider'])) {
     $providerGet = $_GET['provider'];
     $_SESSION['provider'] = $providerGet;
} else if (!empty($_SESSION['provider'])) {
     $providerGet = $_SESSION['provider'];
}
?>

<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<!--[if lt IE 9]>
	<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/html5.js"></script>
	<![endif]-->
	<script>(function(){document.documentElement.className='js'})();</script>
	<?php wp_head(); ?>


	<script src="//use.typekit.net/cxz1qxs.js"></script>
	<script>try{Typekit.load();}catch(e){}</script></head>

	<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/bigslide.js"></script>
	<script>
	jQuery(document).ready(function(){
	  jQuery('#fixedmenulink').bigSlide();
	})
	</script>

<body <?php body_class(); ?>>
<div id="page" class="hfeed site">

<div id="wrapper">

<div id="fixedmenu"><div class="inside">

		<?php get_sidebar(); ?>

		<h2>O Szybkiej Fakturze</h2>

		<ul>
			<li><a href="/index.php" class="link">Strona główna</a></li>
			<li><a href="/fakturowanie.php" class="link">Wystawiaj faktury</a></li>
			<li><a href="/magazyn.php" class="link">Zarządzaj magazynem</a></li>
			<li><a href="/ksiegowosc-online.php" class="link">Rozliczaj samodzielnie</a></li>
			<li><a href="/ksieguj-z-biurem.php" class="link">Księguj z biurem</a></li>
			<li class="right"><a href="/dla-biur-rachunkowych.php" class="link">Dla biur rachunkowych</a></li>
		</ul>

		<ul class="buttons">
			<li><a href="https://app.szybkafaktura.pl/auth/login?app=skto<?php echo ($providerGet !== null) ? '&provider='.$providerGet : '' ?>" class="medium button blue login">Zaloguj się</a></li>
			<li><a href="https://app.szybkafaktura.pl/auth/new-register?app=skto<?php echo ($providerGet !== null) ? '&provider='.$providerGet : '' ?>" class="medium button green signup">Załóż konto</a></li>
		</ul>
		
		<ul>
			<li><a href="/blog" class="link">Blog</a></li>
			<li><a href="/cennik.php" class="link">Cennik</a></li>
			<li><a href="http://pomoc.szybkafaktura.pl" class="link">Pomoc</a></li>
			<li><a href="/o-nas.php" class="link">O nas</a></li>
			<li><a href="https://app.szybkafaktura.pl/user-files/branding_files/regulamin_app_szybkafaktura_pl.pdf?app=skto" class="link">Regulamin</a></li>
			<li><a href="/program-partnerski.php" class="link">Program partnerski</a></li>	
			<li><a href="/kontakt.php" class="link">Kontakt</a></li>
		</ul>
	
</div></div>

			<header>
				<div class="dark">
					<div class="container">
						<div class="inside">
							
							<a href="/index.php" id="logo"  alt="" ><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/logoSzybkaFakturaWhite.svg" /></a>
							<ul class="nav">
								<li class="active"><a href="/blog" class="link">Blog</a></li>
								<li><a href="/cennik.php" class="link">Cennik</a></li>
								<li><a href="http://pomoc.szybkafaktura.pl" class="link">Pomoc</a></li>
								<li><a href="/kontakt.php" class="link">Kontakt</a></li>
								<li><a href="https://app.szybkafaktura.pl/auth/login?app=skto<?php echo ($providerGet !== null) ? '&provider='.$providerGet : '' ?>" class="medium button blue login newButtonLogin">Zaloguj się</a></li>
								<li><a href="https://app.szybkafaktura.pl/auth/new-register?app=skto<?php echo ($providerGet !== null) ? '&provider='.$providerGet : '' ?>" class="medium button green signup newButtonsignup">Załóż darmowe konto</a></li>
							</ul>
							
							<a href="#fixedmenu" id="fixedmenulink" class="medium button blue menu"></a>

								
						</div>
					</div>
				</div>
				<div class="light">
					<div class="container">
						<div class="inside">
							
							<ul class="nav">
								<li><a href="/index.php" class="home">&nbsp;</a></li>
								<li><a href="/wystawianie-faktur.php" class="link">Wystawiaj faktury</a></li>
								<li><a href="/magazyn.php" class="link">Zarządzaj magazynem</a></li>
								<li><a href="/ksiegowosc-online.php" class="link">Rozliczaj samodzielnie</a></li>
								<li><a href="/ksieguj-z-biurem.php" class="link">Księguj z biurem</a></li>
								<li class="right"><a href="/dla-biur-rachunkowych.php" class="link">Dla biur rachunkowych</a></li>
							</ul>
							
						</div>
					</div>
				</div>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1326744-3', 'auto');
  ga('send', 'pageview');

</script>
<!-- BEGIN callpage.io widget -->
<script type="text/javascript">var __cp={"id":"IxFKC_w5eiWT4ezQB6c_HIEVT_bEQBSs2ig2OT9NBg8","version":"1.1"};(function(window,document){var cp=document.createElement('script');cp.type='text/javascript';cp.async=true;cp.src="++cdn-widget.callpage.io+build+js+callpage.js".replace(/[+]/g,'/').replace(/[=]/g,'.');var s=document.getElementsByTagName('script')[0];s.parentNode.insertBefore(cp,s);if(window.callpage){alert('You could have only 1 CallPage code on your website!');}else{window.callpage=function(method){if(method=='__getQueue'){return this.methods;}
else if(method){if(typeof window.callpage.execute==='function'){return window.callpage.execute.apply(this,arguments);}
else{(this.methods=this.methods||[]).push({arguments:arguments});}}};window.callpage.__cp=__cp;window.callpage('api.button.autoshow');}})(window,document);</script>
<!-- END callpage.io widget -->

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-P6M94D3');</script>
<!-- End Google Tag Manager -->
			</header>

				<section id="content">
					
					<div class="container">
						<div class="inside">

