<?php
$sessionId = session_id();

if(empty($sessionId)) {
      @session_start();
}

$providerGet = null;
if (!empty($_GET['provider'])) {
     $providerGet = $_GET['provider'];
     $_SESSION['provider'] = $providerGet;
} else if (!empty($_SESSION['provider'])) {
     $providerGet = $_SESSION['provider'];
}
?>

<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "site-content" div and all content after.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>

	</div></div><!-- .site-content -->

	<div id="sidebar">
		<?php include( TEMPLATEPATH . '/sidebar.php'); ?>
	</div>
	
	</div></div></div>


	<footer>
		<div class="pkobp">
			<div class="container">
				<div class="inside">
					
					<figure></figure>
					
					<div class="text">
						<h2>Atrakcyjna promocja dla klientów PKO BP.</h2>
						<h3>Otwórz rachunek i otrzymaj dostęp do szybkafaktura.pl na <span>6 miesięcy za 0 zł!</span></h3>
					</div>
					
                                        <a href="https://www.pkobp.pl/wnioski/rachunki/msp_konto_1?context=KURIER" target="_blank" class="small button blue"><span></span><label>Dowiedz się więcej</label></a>
					
				</div>
			</div>						
		</div>
		<div class="dark">
			<div class="container">
				<div class="inside">
					
					<div class="row">
															
						<ul>
							<li><a href="/cennik.php" class="link">Cennik</a></li>
							<li><a href="/o-nas.php" class="link">O nas</a></li>
							<li><a href="http://pomoc.szybkafaktura.pl" class="link">Pomoc</a></li>
							<li><a href="http://szybkafaktura.pl/Regulamin_szybkafaktura.pdf" class="link">Regulamin</a></li>
							<li><a href="http://szybkafaktura.pl/Polityka_prywatnosci_szybkafaktura.pdf" class="link">Polityka prywatności</a></li>
							<li><a href="https://app.szybkafaktura.pl/auth/login?app=skto<?php echo ($providerGet !== null) ? '&provider='.$providerGet : '' ?>" class="link">Logowanie</a></li>
							<li><a href="https://app.szybkafaktura.pl/auth/new-register?app=skto<?php echo ($providerGet !== null) ? '&provider='.$providerGet : '' ?>" class="link">Rejestracja</a></li>	
							<li><a href="/program-partnerski.php" class="link">Program partnerski</a></li>	
						</ul>			

						<div class="text">
							<div class="copy">Copyright (c) 2007-2018.
								<br>All rights reserved szybkafaktura.pl</div>
							<div class="true">Designed in <a href="#">true</a></div>
						</div>
						
					</div>

					<div class="logos">
						
						<ul>
							<li class="logoSkanuj"><figure><a href="http://www.skanuj.to" target="_blank"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/logoSkanuj.svg" /></a></figure></li>
							<li class="logoXenga"><figure><a href="http://www.xenga.pl" target="_blank"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/logoXenga.svg" /></a></figure></li>
							<li class="logoSprawdzonyKsiegowy"><a href="http://www.sprawdzonyksiegowy.pl" target="_blank"><figure><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/logoSprawdzonyKsiegowy.svg" /></a></figure></li>
					
							<li class="logoCloudPlanet"><figure><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/logoCloudPlanet.svg" /></figure></li>
							<li class="logoUE right"><figure><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/ue.png" /></figure></li>
						</ul>
						
					</div>
					
				</div>
			</div>
		</div>
	</footer>

</div>

<?php // wp_footer(); ?>

</body>
</html>
