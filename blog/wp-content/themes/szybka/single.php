<?php
$sessionId = session_id();

if(empty($sessionId)) {
      @session_start();
}

$providerGet = null;
if (!empty($_GET['provider'])) {
     $providerGet = $_GET['provider'];
     $_SESSION['provider'] = $providerGet;
} else if (!empty($_SESSION['provider'])) {
     $providerGet = $_SESSION['provider'];
}
?>
<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>

	<div id="left">
		<div class="inside">

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php
		// Start the loop.
		while ( have_posts() ) : the_post();

			/*
			 * Include the post format-specific template for the content. If you want to
			 * use this in a child theme, then include a file called called content-___.php
			 * (where ___ is the post format) and that will be used instead.
			 */
			get_template_part( 'content', get_post_format() ); ?>
			
				<section id="signup">
		
					<div class="container">
						<div class="inside">
							<p>Wystawiaj faktury w naszym systemie</p><a href="https://app.szybkafaktura.pl/auth/new-register?app=skto<?php echo ($providerGet !== null) ? '&provider='.$providerGet : '' ?>" class="medium button green signup newButtonsignup">Załóż konto</a><p>Wypróbuj przez 45 dni za darmo!</p>
						</div>
					</div>
					
				</section>

			<?php
				
			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

			// Previous/next post navigation.
			the_post_navigation( array(
				'next_text' => 
					'<span class="screen-reader-text">' . __( 'Next post:', 'twentyfifteen' ) . '</span> ' .
					'<span class="post-title">%title</span>',
				'prev_text' => 
					'<span class="screen-reader-text">' . __( 'Previous post:', 'twentyfifteen' ) . '</span> ' .
					'<span class="post-title">%title</span>',
			) );
			
			

		// End the loop.
		endwhile;
		
			echo "<div class='related-posts'>";
			echo do_shortcode( '[bws_related_posts]' );		
			echo "</div>";
		
		?>

		
		</main><!-- .site-main -->
	</div><!-- .content-area -->

<?php get_footer(); ?>
