<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php
	$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'big' );
	$url = $thumb['0'];
	?>
	<?php if (!is_single()&&has_post_thumbnail()): ?>
	<figure style="background-image:url(<?=$url?>);" class="small-thumb">&nbsp;</figure>
	<?php endif; ?>	
	
	<div class="text">

	

	<div class="entry-header">
		<?php
			if ( is_single() ) :
				the_title( '<h1 class="entry-title">', '</h1>' );
			else :
				the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' );
			endif;
		?>
	</div><!-- .entry-header -->
	
	<div class="meta">
		<?php if (is_single()): echo get_avatar(get_the_author_meta('ID'),64); endif; ?>
		<span class="author"><?php echo get_the_author(); ?></span>
		<span class="date"><?php echo get_the_date("d/m/Y"); ?></span>
	  <span class="comment"><?php comments_number( 'brak odpowiedzi', 'jedna odpowiedź', 'liczba odpowiedzi: %s' ); ?></span>
	</div>

	<?php if (is_single()&&has_post_thumbnail()): ?>
	<figure class="big-poster"><div style="background-image:url(<?=$url?>);">&nbsp;</div></figure>
	<?php endif; ?>	

	<div class="entry-content">

		<?php 
			if (is_single()):
				the_content();
			else: 
				the_excerpt();
				 ?><a href="<?php the_permalink(); ?>" class="verysmall button blue login newMoreButton">Więcej</a><?php
			endif;
		?>
		

	</div><!-- .entry-content -->

	<?php
		// Author bio.
		if ( is_single() && get_the_author_meta( 'description' ) ) :
			get_template_part( 'author-bio' );
		endif;
	?>

	<div class="entry-footer">
		<?php edit_post_link( __( 'Edit', 'twentyfifteen' ), '<span class="edit-link">', '</span>' ); ?>
	</div><!-- .entry-footer -->
	</div>
</article><!-- #post-## -->
