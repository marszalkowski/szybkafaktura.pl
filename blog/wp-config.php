<?php
/**
 * Podstawowa konfiguracja WordPressa.
 *
 * Ten plik zawiera konfiguracje: ustawień MySQL-a, prefiksu tabel
 * w bazie danych, tajnych kluczy i ABSPATH. Więcej informacji
 * znajduje się na stronie
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Kodeksu. Ustawienia MySQL-a możesz zdobyć
 * od administratora Twojego serwera.
 *
 * Ten plik jest używany przez skrypt automatycznie tworzący plik
 * wp-config.php podczas instalacji. Nie musisz korzystać z tego
 * skryptu, możesz po prostu skopiować ten plik, nazwać go
 * "wp-config.php" i wprowadzić do niego odpowiednie wartości.
 *
 * @package WordPress
 */

define('WP_HOME','http://szybkafaktura.pl/blog/');
define('WP_SITEURL','http://szybkafaktura.pl/blog/');


// ** Ustawienia MySQL-a - możesz uzyskać je od administratora Twojego serwera ** //
/** Nazwa bazy danych, której używać ma WordPress */
define('DB_NAME', 'cloudpl2_szfb');

/** Nazwa użytkownika bazy danych MySQL */
define('DB_USER', 'cloudpl2_szfb');

/** Hasło użytkownika bazy danych MySQL */
define('DB_PASSWORD', 'eFOL05Lf');

/** Nazwa hosta serwera MySQL */
define('DB_HOST', 'localhost');

/** Kodowanie bazy danych używane do stworzenia tabel w bazie danych. */
define('DB_CHARSET', 'utf8');

/** Typ porównań w bazie danych. Nie zmieniaj tego ustawienia, jeśli masz jakieś wątpliwości. */
define('DB_COLLATE', '');

/**#@+
 * Unikatowe klucze uwierzytelniania i sole.
 *
 * Zmień każdy klucz tak, aby był inną, unikatową frazą!
 * Możesz wygenerować klucze przy pomocy {@link https://api.wordpress.org/secret-key/1.1/salt/ serwisu generującego tajne klucze witryny WordPress.org}
 * Klucze te mogą zostać zmienione w dowolnej chwili, aby uczynić nieważnymi wszelkie istniejące ciasteczka. Uczynienie tego zmusi wszystkich użytkowników do ponownego zalogowania się.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'c1f-M60;k#s(r@!MEqFGym=NOzfD<n2fJ|-kW!irW<G3=hCy,d|=oX3{FruCe?YU');
define('SECURE_AUTH_KEY',  'BVFFt[#vA9I`t`%wQ_yaQ|vlH.JjG%jV9v.^_3?},H:3phFlZ[+5#`d1wxhyj4])');
define('LOGGED_IN_KEY',    '|AqI_*.+DCu]k0Jk+heCuXzm;yT&,R@7pzEU4eacjb},;grE)<MQvi$vPb-CY|cO');
define('NONCE_KEY',        'h1tb!#>/|0AfVdy?>so8G!{H1.RaDIB5e P:y|Or7?h^+[bfc0~J2nD$amQo2ca}');
define('AUTH_SALT',        '@+=#;mhl:qG/}wDh*oVR|K^&[)p,RlGIU)3+]Y(ntmIq5jSz8+8&87Mtj4RYxqf=');
define('SECURE_AUTH_SALT', 'V1PV=(*:wZ]nBvy+8yvCAE>(PA.7_|3tQO+}v#lMeT@^Y5Fr-rV>kHhp|gqeOMn/');
define('LOGGED_IN_SALT',   'J%a7oK$g*~&|_P-|(BG^d7qppcy(XouOi&4OC@f-TSeNRzJHQ!j}K)&o.p;l-2BV');
define('NONCE_SALT',       'xuz$DRE}W^9ioE(eur@k=O.c/MEZ6HtvKKv:s7 `3+28He#6ap|{/4f@gaf<8-nA');

/**#@-*/

/**
 * Prefiks tabel WordPressa w bazie danych.
 *
 * Możesz posiadać kilka instalacji WordPressa w jednej bazie danych,
 * jeżeli nadasz każdej z nich unikalny prefiks.
 * Tylko cyfry, litery i znaki podkreślenia, proszę!
 */
$table_prefix  = 'wp_';

/**
 * Dla programistów: tryb debugowania WordPressa.
 *
 * Zmień wartość tej stałej na true, aby włączyć wyświetlanie ostrzeżeń
 * podczas modyfikowania kodu WordPressa.
 * Wielce zalecane jest, aby twórcy wtyczek oraz motywów używali
 * WP_DEBUG w miejscach pracy nad nimi.
 */
define('WP_DEBUG', false);

/* To wszystko, zakończ edycję w tym miejscu! Miłego blogowania! */

/** Absolutna ścieżka do katalogu WordPressa. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Ustawia zmienne WordPressa i dołączane pliki. */
require_once(ABSPATH . 'wp-settings.php');
