<?php
$providerGet = null;
if (!empty($_GET['provider'])) {
     $providerGet = $_GET['provider'];
     $_SESSION['provider'] = $providerGet;
} else if (!empty($_SESSION['provider'])) {
     $providerGet = $_SESSION['provider'];
}
?>

<div id="fixedmenu"><div class="inside">

		<ul>
		
			<li><a href="http://szybkafaktura.pl" class="link">Strona główna</a></li>
			<li><a href="/wystawianie-faktur.php" class="link">Wystawiaj faktury</a></li>
			<li><a href="/magazyn.php" class="link">Zarządzaj magazynem</a></li>
			<li><a href="/ksiegowosc-online.php" class="link">Rozliczaj samodzielnie</a></li>
			<li><a href="/ksieguj-z-biurem.php" class="link">Księguj z biurem</a></li>
			<li class="right"><a href="/dla-biur-rachunkowych.php" class="link">Dla biur rachunkowych</a></li>
		</ul>

		<ul class="buttons">
			<li><a href="https://app.szybkafaktura.pl/auth/login?app=skto<?php echo ($providerGet !== null) ? '&provider='.$providerGet : '' ?>" class="medium button blue login"><label>Zaloguj się</label></a></li>
			<li><a href="https://app.szybkafaktura.pl/auth/new-register?app=skto<?php echo ($providerGet !== null) ? '&provider='.$providerGet : '' ?>" class="medium button green signup"><label>Załóż konto</label></a></li>
		</ul>
		
		<ul>
		<li><a href="/wszystkie_funkcje_systemu.php" class="link">Funkcje</a></li>
		<li><a href="/cennik.php" class="link">Cennik</a></li>
			<li><a href="/blog" class="link">Blog</a></li>
			<li><a href="http://pomoc.szybkafaktura.pl" class="link">Pomoc</a></li>
			<li><a href="/o-nas.php" class="link">O nas</a></li>
			<li><a href="https://app.szybkafaktura.pl/user-files/branding_files/regulamin_app_szybkafaktura_pl.pdf?app=skto" class="link">Regulamin</a></li>
			<li><a href="/program-partnerski.php" class="link">Program partnerski</a></li>	
			<li><a href="/kontakt.php" class="link">Kontakt</a></li>
		</ul>
	
</div></div>


				<header>
					<div class="dark">
						<div class="container">
							<div class="inside">
								
								<a href="http://szybkafaktura.pl" id="logo"  alt="" ><img src="img/logoSzybkaFakturaWhite.svg" /></a>
								<ul class="nav">
								<li><a href="/wszystkie_funkcje_systemu.php" class="link">Funkcje</a></li>
								<li><a href="/cennik.php" class="link">Cennik</a></li>
								<li><a href="/blog" class="link">Blog</a></li>
								<li><a href="http://pomoc.szybkafaktura.pl" class="link">Pomoc</a></li>
									<li><a href="/kontakt.php" class="link">Kontakt</a></li>
                                                                        <li><a href="https://app.szybkafaktura.pl/auth/login?app=skto<?php echo ($providerGet !== null) ? '&provider='.$providerGet : '' ?>" class="medium button blue login newButtonLogin">Zaloguj się</a></li>
									<li><a href="https://app.szybkafaktura.pl/auth/new-register?app=skto<?php echo ($providerGet !== null) ? '&provider='.$providerGet : '' ?>" class="medium button green signup newButtonsignup">Załóż darmowe konto</a></li>
								</ul>
									
								<a href="#fixedmenu" id="fixedmenulink" class="medium button blue menu"><label></label></a>
									
							</div>
						</div>
					</div>
					<div class="light">
						<div class="container">
							<div class="inside">
								
								<ul class="nav">
<!--									<li><a href="/index.php" class="home">&nbsp;</a></li>-->
									<li><a href="/wystawianie-faktur.php" class="link">Wystawiaj faktury</a></li>
									<li><a href="/magazyn.php" class="link">Zarządzaj magazynem</a></li>
									<li><a href="/ksiegowosc-online.php" class="link">Rozliczaj samodzielnie</a></li>
									<li><a href="/ksieguj-z-biurem.php" class="link">Księguj z biurem</a></li>
									<li class="right"><a href="/dla-biur-rachunkowych.php" class="link">Dla biur rachunkowych</a></li>
								</ul>
								
							</div>
						</div>
					</div>
					<!-- BEGIN callpage.io widget -->
<script type="text/javascript">var __cp={"id":"IxFKC_w5eiWT4ezQB6c_HIEVT_bEQBSs2ig2OT9NBg8","version":"1.1"};(function(window,document){var cp=document.createElement('script');cp.type='text/javascript';cp.async=true;cp.src="++cdn-widget.callpage.io+build+js+callpage.js".replace(/[+]/g,'/').replace(/[=]/g,'.');var s=document.getElementsByTagName('script')[0];s.parentNode.insertBefore(cp,s);if(window.callpage){alert('You could have only 1 CallPage code on your website!');}else{window.callpage=function(method){if(method=='__getQueue'){return this.methods;}
else if(method){if(typeof window.callpage.execute==='function'){return window.callpage.execute.apply(this,arguments);}
else{(this.methods=this.methods||[]).push({arguments:arguments});}}};window.callpage.__cp=__cp;window.callpage('api.button.autoshow');}})(window,document);</script>
<!-- END callpage.io widget -->
				</header>
