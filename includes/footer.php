<?php
$providerGet = null;
if (!empty($_GET['provider'])) {
     $providerGet = $_GET['provider'];
     $_SESSION['provider'] = $providerGet;
} else if (!empty($_SESSION['provider'])) {
     $providerGet = $_SESSION['provider'];
}
?>
<footer>
<!--
					<div class="pkobp">
						<div class="container">
							<div class="inside">
								
								<figure></figure>
								
								<div class="text">
									<h2>Atrakcyjna promocja dla klientów PKO BP.</h2>
									<h3>Otwórz rachunek i otrzymaj dostęp do szybkafaktura.pl na <span>6 miesięcy za 0&nbsp;zł!</span></h3>
								</div>
								
								<a href="https://www.pkobp.pl/wnioski/rachunki/msp_konto_1?context=KURIER" target="_blank" class="small button blue"><span></span><label>Dowiedz się więcej</label></a>
								
							</div>
						</div>						
					</div>
					-->
                    
					<div class="dark">
						<div class="container">
							<div class="inside">
								
								<div class="row">
																		
									<ul>
										<li><a href="/cennik.php" class="link">Cennik</a></li>
										<li><a href="/o-nas.php" class="link">O nas</a></li>
										<li><a href="http://pomoc.szybkafaktura.pl" class="link">Pomoc</a></li>
										<li><a href="http://szybkafaktura.pl/Regulamin_szybkafaktura.pdf" class="link">Regulamin</a></li>
                                        <li><a href="http://szybkafaktura.pl/Polityka_prywatnosci_szybkafaktura.pdf" class="link">Polityka prywatności</a></li>
										<li><a href="https://app.szybkafaktura.pl/auth/login?app=skto<?php echo ($providerGet !== null) ? '&provider='.$providerGet : '' ?>" class="link">Logowanie</a></li>
										<li><a href="https://app.szybkafaktura.pl/auth/new-register?app=skto<?php echo ($providerGet !== null) ? '&provider='.$providerGet : '' ?>" class="link">Rejestracja</a></li>	
										<li><a href="/program-partnerski.php" class="link">Program partnerski</a></li>	
										<li>Aplikacja mobilna do pobrania: <a href="https://itunes.apple.com/pl/app/szybkafaktura.pl/id1049729347?mt=8"><img src="img/iOS-32.png"></a> / <a href="https://play.google.com/store/apps/details?id=pl.cloud_planet.szybkafaktura
"><img src="img/android-32.png"></a></li>
									</ul>			

									<div class="text">
										<div class="copy">Wszystkie prawa zastrzeżone
											<br class="hide-on-mobile">© 2012-2020 na szybkafaktura.pl</div>
										
									</div>
									
								</div>

								<div class="logos">
								
									
									<ul>
									<li>
										<li class="logoSkanuj"><figure><a href="https://www.skanuj.to" target="_blank"><img src="img/logoSkanuj.png" /></a></figure></li>
								
										<li class="logoSprawdzonyKsiegowy"><a href="https://www.sprawdzonyksiegowy.pl" target="_blank"><figure><img src="img/logoSprawdzonyKsiegowy.png" /></a></figure></li>
                                                                             <li class="logoCloudPlanet"><figure><a href="http://cloud-planet.pl" target="_blank"><img src="img/logoCloudPlanet.png" /></a></figure></li><br><br><br>
										<div id="center2">
										<li><img src="img/ue3.png" /></li>
										</div>
                                        
									</ul>
									
								</div>
								
							</div>
						</div>
					</div>
				</footer>

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.1.min.js"><\/script>')</script>

	<script src='js/jquery.modal.min.js'></script>

        <script src="js/plugins.js"></script>
        <script src="js/cycle2.js"></script>
        <script src="js/stickit.js"></script>
        <script src="js/magnific.js"></script>
				<script src="js/jquery.tooltipster.min.js"></script>
        <script src="js/bigslide.js"></script>
        <script src="js/main.js"></script>
