<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'cloudpl2_wp1');

/** MySQL database username */
define('DB_USER', 'cloudpl2_wp1');

/** MySQL database password */
define('DB_PASSWORD', 'Q,g6RWCs)avqd2a]wU)17],5');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'WwCwSgDR7CVLHU40vcSHmjAzPkdfi9tAXZAQJCCDggvCuwd59hJoj67N941ixWBN');
define('SECURE_AUTH_KEY',  'REdL302r91jNvOr3cfq4I6dViyPrLAQtwCV8mwKOShbsrzPen3Cxs5dUA63yFgwk');
define('LOGGED_IN_KEY',    'Xm4C6SIPqruUImDRtlDyzq5Zrll8CAQCMl1pAXj3QbKawNDN0iXAQQMdtjJWUFmd');
define('NONCE_KEY',        'qy0JZVki3CF7i0Mpvtf5I80nySxOOwC9z4IEGKuAsmPKBUUF5FwhmD8pSrT2fWo0');
define('AUTH_SALT',        'DM65gavNa9P3FNT8JMHKoWWv2QoScab2Coy0IikXauVhL6H0aab961RSO3w9Jezh');
define('SECURE_AUTH_SALT', 'YW0Vy0Vu944efY2pTICB17OgardJsv2bx3myiFujRGMbBZHnwQelUyIYtiz8Blo9');
define('LOGGED_IN_SALT',   'o4xGBarlcz8dPLxcJftlTyiDJiJCckY5dS24uDhXcuRabBJ0du5sMmi1UrS2zb2r');
define('NONCE_SALT',       '7ztJbVcdmuuMB3aCohBQ9uXiAEiNhQY4qTL0kL0CMRrlVrLvLEzdCwNmSXklYlRm');

/**
 * Other customizations.
 */
define('FS_METHOD','direct');define('FS_CHMOD_DIR',0755);define('FS_CHMOD_FILE',0644);
define('WP_TEMP_DIR',dirname(__FILE__).'/wp-content/uploads');

/**
 * Turn off automatic updates since these are managed upstream.
 */
define('AUTOMATIC_UPDATER_DISABLED', true);


/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
