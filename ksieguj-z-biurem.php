<?php
$sessionId = session_id();

if(empty($sessionId)) {
     @session_start();
}

$providerGet = null;
if (!empty($_GET['provider'])) {
     $providerGet = $_GET['provider'];
     $_SESSION['provider'] = $providerGet;
} else if (!empty($_SESSION['provider'])) {
     $providerGet = $_SESSION['provider'];
}
?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Księguj z biurem rachunkowym | szybkafaktura.pl</title>
        <meta name="keywords" content="faktura online, fakturowanie online, faktura vat, faktura vat online" />
<meta name="description" content="Zupełnie nowe rozwiązanie na rynku usług księgowych, łączące zalety dwóch rozwiązań: księgowości online oraz tradycyjnej obsługi księgowej" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="canonical" href="http://szybkafaktura.pl/ksieguj-z-biurem.php" />
				<?php include_once("includes/head.php") ?>
    </head>
    <body class="product-page" id="all-inclusive">

				<?php include_once("includes/header.php") ?>

				<section id="intro">
					
					<div class="container">
						<div class="inside">
						
							<h1>Księguj z biurem</h1>
							
							<p>Jeśli masz księgowego i męczy Cię comiesięczne dostarczanie dokumentów dodaj go do swojego konta w systemie.</p>
							<p>Dzięki temu rozwiązaniu Twój księgowy będzie mógł w dowolnej chwili sprawdzić poprawność Twoich dokumentów, zaksięgować je lub wyeksportować do swojego programu księgowego.
To oszczędność czasu i wygoda dla Ciebie oraz Twojego księgowego.</p>

							
							<div id="all-inclusive">
								
								
							</div>

						
						</div>
					</div>
					
				</section>
				
				<section id="features">
					
					<div class="container">
						<div class="inside">
						
							<div class="feature" id="latwosc-obslugi">
																
								<figure><img src="img/image-10.png" alt="Wysoka jakość usług księgowych" ></figure>

								<div class="text">

									<h2>Łatwa komunikacja z księgowym</h2>
									<p>Już nie musisz pamiętać o terminowym przesyłaniu dokumentów do księgowego.
W każdej chwili będzie mógł samodzielnie pobrać lub zaksięgować wszystkie niezbędne dokumenty.</p>
									
								</div>
						
							</div>

							<div class="feature" id="automatyczne-przetwarzanie">
																
								

								<div class="text">

									<h2>Rozliczenia podatkowe zawsze na czas</h2>
									<p>Twój księgowy przygotuje wszystkie rozliczenia (VAT, KPiR, ZUS), wyśle deklaracje i podpowie, jak korzystnie dokonać rozliczeń, a system szybkafaktura.pl automatycznie przypomni o terminach płatności.</p>
									
								</div>

								<figure class="hide-on-phone"><img src="img/image-11.png" alt="Fachowa pomoc księgowa" ></figure>
						
							</div>

							<div class="feature" id="kontrola-finansow">
																
								<figure><img src="img/image-12.png" alt="Kontrola nad działalnością i finansami firmy" ></figure>

								<div class="text">
									<h2>Kontrola nad działalnością i finansami firmy</h2>
									<p>W szybkafaktura.pl możesz oznaczać płatności i wysyłać przypomnienia o niezapłaconych fakturach do swoich kontrahentów. System zapewnia możliwość bieżącej komunikacji z biurem rachunkowym, dostęp do wszystkich danych księgowych, kadrowych, płacowych i wiele innych.</p>
								</div>
						
							</div>

						
						</div>
					</div>
					
				</section>
				<section id="details">
				<div class="container">
						<div class="inside">
							<h2>Jak dodać księgowego?</h2>
							<ul>
<li>1. W Ustawienia odszukaj sekcję Użytkownicy, następnie kliknij przycisk Dodaj użytkownika z uprawnieniami pełnomocnik/księgowa.</li>
<li>2. Podaj adres email księgowego oraz jego imię i nazwisko.</li>
<li>3. Twój księgowy otrzyma wiadomość email, za pomocą której utworzy swoje hasło logowania.</li>

							</ul>
</div></div></section>
				
				<section id="signup">
					
					<div class="container">
						<div class="inside">
							<p>Dołącz do grona zadowolonych klientów.</p><a href="https://app.szybkafaktura.pl/auth/new-register?app=skto<?php echo ($providerGet !== null) ? '&provider='.$providerGet : '' ?>" class="medium button green signup"><span></span><label>Załóż konto</label></a><p>Wypróbuj przez 45 dni za darmo!</p>
						</div>
					</div>
					
				</section>
				
				<section id="details">
					
					<div class="container">
						<div class="inside">
						
							<h2>Najważniejsze funkcje</h2>
							
							<div class="row">
							
							<ul class="column">
								<li>eksport danych do programów księgowych</li>
								<li>szybkie i łatwe wystawianie dokumentów</li>
								<li>wprowadzanie i digitalizacja dokumentów kosztowych, możliwość załączenia pdf</li>
								<li>automatyczne rozpoznawanie danych z faktur</li>
								<li>archiwum dokumentów</li>
								<li>kontrola płatności faktur</li>
								<li>wysyłanie przypomnień do kontrahentów</li>
							</ul>
							
							<ul class="column">
								<li>baza kontrahentów, towarów i usług</li>
								<li>tworzenie kont wielu użytkownikom</li>
								
								<li>podgląd w rozliczenia biura rachunkowego (amortyzację ŚT, kilometrówki, delegację, listę płac), deklaracje podatkowe</li>
								<li>ewidencja sprzedaży i zakupu</li>
							</ul>
							
							</div>
						
						</div>
					</div>					
					
				</section>

				<section id="quotes">
					
					<div class="container">
						<div class="inside">
						
							<ul class="quotes cycle-slideshow" data-cycle-timeout="2000" data-cycle-slides="> li.quote">
								
								<li class="quote">
									<span class="quote-content">Odkąd pracuję w systemie szybkafaktura.pl, nasi klienci doceniają zalety współpracy online, zwłaszcza prosty moduł do fakturowania i automatyczne przekazywanie dokumentów do biura.</span>
									<span class="quote-author">Paweł Pec, Doradca Podatkowy, numer 05502 Biuro Rachunkowo-Konsultingowe MAX, Poznań</span>
								</li>

						    <div class="cycle-pager"></div>		

							</ul>
						
						</div>
					</div>					
					
				</section>

				<?php include_once("includes/footer.php") ?>

        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='//www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-1326744-3');
			ga('set', 'contentGroup1', 'Grupa www kod'); 
			ga('send','pageview');
        </script>
    </body>
</html>
