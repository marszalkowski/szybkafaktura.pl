var allowSubmit = true;

function getTextValue(fname) {
	return $("#" + fname).val();
}

function clear_field(fname) {
	$("#" + fname).val('');
}

function notify_field(fname, fmessage) {
	$("#" + fname).addClass("input-error-notify");
	$("#" + fname + "-error > span").text(fmessage);
}

function notify_field_with_error(fname, efname, fmessage) {
	$("#" + fname).addClass("input-error-notify");
	$("#" + efname + " > span").text(fmessage);
}

function turn_off_notify_field(fname) {
	$("#" + fname).removeClass("input-error-notify");
	$("#" + fname + "-error > span").text("");
}

function process_response(responseXML) {
	var error_dict = {};
	var resp_code = $('code', responseXML).text();
	$(responseXML).find('error').each(function () {
		var err_code = $(this).attr('code');
		var err_message = $(this).text();
		error_dict[err_code] = err_message;
	});
	if (resp_code == 'SUCCESSFULL') {
        var query = '';
        var provider = getTextValue('provider');
        if(provider !== ''){
            query = '?provider='+provider;
            var user = $('user', responseXML).text();
            if(user !== ''){
                query = query + '&APPID='+user;
            }
        }
		turn_off_notify_field("email");
		turn_off_notify_field("password");
		turn_off_notify_field("repeated-password");
		turn_off_notify_field("rules-acceptance");
		$(window.location).attr('href', getAppUrl() + '/rejestracja-zakonczona'+query);
	} else if (resp_code == 'VALIDATION_ERROR' || resp_code == 'UNKNOWN_ERROR') {
		document.getElementById("submit-form-button").disabled = false;
		if (error_dict['email_required']) {
			notify_field("email", "Podaj swój e-mail");
		} else if (error_dict['email_invalid']) {
			notify_field("email", "E-mail ma niewłaściwy format");
		} else if (error_dict['email_duplicated']) {
			notify_field("email", "E-mail został już wcześniej użyty");
		} else if (error_dict['pl.mojebiuro.remote.exception.business.DuplicateEmailInDatabaseException']) {
			notify_field("email", "E-mail został już wcześniej użyty");
		} else if (resp_code == 'UNKNOWN_ERROR' && !$.isEmptyObject(error_dict)) {
			notify_field("email", "Błąd podczas wysyłania formularza. Spróbuj ponownie");
		} else {
			turn_off_notify_field("email");
		}
		if (error_dict['password_required']) {
			notify_field("password", "Podaj swoje hasło");
		} else if (error_dict['password_tooShort']) {
			notify_field("password", "Hasło powinno mieć 6 znaków");
		} else {
			turn_off_notify_field("password");
		}
		if (error_dict['repeatedPassword_required']) {
			notify_field("repeated-password", "Powtórz swoje hasło");
		} else if (error_dict['repeatedPassword_notMatch']) {
			notify_field("repeated-password", "Hasła nie są zgodne");
		} else {
			turn_off_notify_field("repeated-password");
		}
		if (error_dict['phone_number_required']) {
			notify_field("phone", "Podaj swój numer telefonu");
		} else if (error_dict['phone_number_invalid']) {
			notify_field("phone", "Numer telefonu ma niewłaściwy format");
		} else {
			turn_off_notify_field("phone");
		}
		if (error_dict['rulesAcceptance_required']) {
			notify_field("rules-acceptance", "Zaakceptuj regulamin");
		} else {
			turn_off_notify_field("rules-acceptance");
		}
	}
	allowSubmit = true;
}

function getAppUrl() {
	var protocol = 'https:';
	if (window.XDomainRequest) {
		protocol = window.location.protocol;
	}
	return protocol + '//app.szybkafaktura.pl';
}


$(document).ready(function () {
//	$("#register-form").submit(function (event) {
//		if (!allowSubmit) {
//			return false;
//		}
//		document.getElementById("submit-form-button").disabled = true;
//		$.ajax({
//			type: 'POST',
//			data: $("#register-form").serialize(),
//			url: getAppUrl() + '/sf/rs/resources/account/extended/register',
//			success: process_response
//		});
//		allowSubmit = false;
//		return false;
//	});
});

