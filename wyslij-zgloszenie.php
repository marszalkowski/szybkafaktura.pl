<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="css/style-sk.css">
<script>


    function validatenip(nip) {
    var nip_bez_kresek = nip.replace(/-/g,"");
    var reg = /^[0-9]{10}$/;
    if(reg.test(nip_bez_kresek) == false) {
    return false;}
    else
    {
    var dig = (""+nip_bez_kresek).split("");
    var kontrola = (6*parseInt(dig[0]) + 5*parseInt(dig[1]) + 7*parseInt(dig[2]) + 2*parseInt(dig[3]) + 3*parseInt(dig[4]) + 4*parseInt(dig[5]) + 5*parseInt(dig[6]) + 6*parseInt(dig[7]) + 7*parseInt(dig[8]))%11;
    if(parseInt(dig[9])==kontrola)
    return true;
    else
    return false;
    }
     
    }

        function validateemail(email) {
    var reg = /^([A-Za-z0-9_\-\.\+])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    if(reg.test(email) == false) {
    return false;}
    else
    {return true;}}
    
            function validatetel(tel) {
    var reg = /^[0-9\+]{8,13}$/;
    if(reg.test(tel) == false) {
    return false;}
    else
    {return true;}}

$(function(){

	$('.sendNotificationMessageSubmit').click(function(){
			$('#sendNotificationMessage').submit();
		});

    $('#sendNotificationMessage').submit(function(){
        $('#error-msg-modal').hide();
        $('#sendNotificationMessage input').removeClass('error-input');
        var errorMsg = new Array();
        var i = 0;
        var error = 0;
        var email = $('#sendNotificationMessage #email').val();
        if(email != '' && !validateemail(email)){
            $('#sendNotificationMessage #email').addClass('error-input');
            error = 1;
            i++;
            errorMsg[i] = 'adres email';
        }
        var userName = $('#sendNotificationMessage #userName').val();
        if(userName.length < 3){
            $('#sendNotificationMessage #userName').addClass('error-input');
            error = 1;
            i++;
            errorMsg[i] = 'imię i nazwisko';
        }

        var phone = $('#sendNotificationMessage #phone').val();
    	phone = phone.replace(/-/g,"");
	   phone = phone.replace(/ /g,"");
        if(!validatetel(phone)){
            $('#sendNotificationMessage #phone').addClass('error-input');
            error = 1;
             i++;
            errorMsg[i] = 'numer telefonu';
        }

        if(error == 1){
            $('#error-msg-modal').html('Niepoprawnie wypełniony formularz, prosimy o uzupełnienie danych:<ul>');
            for (var j = 1; j<=i; j++){
                var html = $('#error-msg-modal').html();
                $('#error-msg-modal').html(html+ ''+'<li>'+errorMsg[j]+'</li>');
            }
            
            var html = $('#error-msg-modal').html();
            console.log(html);
            $('#error-msg-modal').html(html+ '</ul>');
            $('#error-msg-modal').show();

            return false;
        }else {
                alert("Wiadomość zostanie wysłana. Dziękujemy za kontakt.");
            return true;
        }
        
        });
});
</script>

<?php 
	$package = $_GET['package'];

	switch($package){
		case "premium":
		$package = 'premium';
		break;

		case "bankowy-ksiegowy":
		$package = 'bankowy-ksiegowy';
		break;

		default:
		$package = 'premium';
		break;
	}
?>

<div class="gray-form-container contact-form-container">
											
							<div class="form">
											
							<form action="/wyslij-zgloszenie-action.php" method="POST" id="sendNotificationMessage" class="contact-form">
				
								<h2>Zostaw swój numer oddzwonimy</h2>
                                 			<input type="hidden" value="<?php echo $package; ?>" name="package" />
								<fieldset class="text half-width">
								
									<label>Twoje imię i nazwisko</label>
									<input type="text" name="userName" id="userName" class="small" />
								
								</fieldset>
								
								
								<fieldset class="text half-width no-margin">
								
									<label>Numer telefonu</label>
									<input type="text" name="phone" id="phone" class="small" />
								
								</fieldset>
								
								<fieldset class="text half-width">
								
									<label>Adres e-mail</label>
									<input type="text" name="email" id="email"  class="small" />
								
								</fieldset>

								<fieldset class="submit half-width no-margin" style="margin-top: 15px;">
								
									<button style="margin-top: 23px;" class="small new-button sendNotificationMessageSubmit emerald button newButtonsignup">Wyślij zgłoszenie</button>
								
								</fieldset>
                                
                                <fieldset class="text full-width">
                                    <div id="error-msg-modal" class="error error-registry"></div>
                                </fieldset>
						
							</form>
							
							</div>
						
						</div>
