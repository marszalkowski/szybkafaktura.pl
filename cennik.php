<?php
$sessionId = session_id();

if (empty($sessionId)) {
     @session_start();
}

$providerGet = null;
if (!empty($_GET['provider'])) {
     $providerGet = $_GET['provider'];
     $_SESSION['provider'] = $providerGet;
} else if (!empty($_SESSION['provider'])) {
     $providerGet = $_SESSION['provider'];
}
?>
<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<head>
     <meta charset="utf-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
     <title>Fakturowanie online – Cennik SzybkaFaktura.pl</title>
     <meta name="keywords" content="faktura online, fakturowanie online, faktura vat, faktura vat online"/>
     <meta name="description" content="Oto ceny usług fakturowania online szybkafaktura.pl"/>
     <meta name="viewport" content="width=device-width, initial-scale=1">
     <link rel="canonical" href="http://szybkafaktura.pl/cennik.php"/>

     <?php include_once("includes/head.php") ?>
     <link rel="stylesheet" href="css/jquery.tooltipster.css">

</head>
<body class="product-page" id="cennik">

<?php include_once("includes/header.php") ?>

<div class="container">
     <div class="inside">

          <h1>Poznaj nasze pakiety usług</h1>
          <div class="signup-first"><a href="https://app.szybkafaktura.pl/auth/new-register?app=skto<?php echo ($providerGet !== null) ? '&provider=' . $providerGet : '' ?>">
                    <button class="big button green button-45-days">Wypróbuj przez 45 dni za&nbsp;darmo</button>
               </a></div>


          <section id="pricelist" class="hide show-on-portrait show-on-tablet show-on-desktop">

               <div class="options">

                    <h3>Wybierz okres <br class="hide show-on-tablet show-on-portrait"/>trwania abonamentu</h3>

                    <div class="option">
                         <label for="one-month">
                              <input type="radio" name="radio" id="one-month" value="one-month"/>
                              <figure></figure>
                              <span><big>1</big>miesiąc</span>
                         </label>
                    </div>
                    <div class="option">
                         <label for="three-months">
                              <input type="radio" name="radio" id="three-months" value="three-months"/>
                              <figure></figure>
                              <span><big>3</big>miesiące</span>
                              <em>5% rabatu</em>
                         </label>
                    </div>
                    <div class="option">
                         <label for="six-months">
                              <input type="radio" name="radio" id="six-months" value="six-months"/>
                              <figure></figure>
                              <span><big>12</big>miesięcy</span>
                              <em>12% rabatu</em>
                         </label>
                    </div>
                    <div class="option">
                         <label for="twelve-months">
                              <input type="radio" name="radio" id="twelve-months" value="twelve-months"/>
                              <figure></figure>
                              <span><big>24</big>miesiące</span>
                              <em>12% rabatu</em>
                         </label>
                    </div>

               </div>

               <div id="table-header">

                    <ul id="header-names">

                         <li class="pakiet-podstawowy">Pakiet<br>Podstawowy</li>
                         <li class="pakiet-pelny">Pakiet<br>Magazyn</li>
                        <li class="pakiet-all-inclusive">Pakiet <br>Pełny</li>

                    </ul>

                    <ul id="header-prices">

                         <li class="pakiet-podstawowy">
                              <span class="price"><em class="zlote">9</em>,<em class="grosze">99</em></span>
                              <span class="monthly">zł / mc</span>
                         </li>

                         <li class="pakiet-pelny">
                              <span class="price"><em class="zlote">29</em>,<em class="grosze">99</em></span>
                              <span class="monthly">zł / mc</span>
                         </li>

                        <li class="pakiet-all-inclusive">
                              <span class="price"><em class="zlote">26</em>,<em class="grosze">39</em></span>
                              <span class="monthly">zł / mc</span>
                         </li>

                    </ul>

               </div>

               <table>
                    <tr class="trzy-gratis">
                         <td></td>
                         <td><em class="trzy-miesiace-gratis">+3 miesiące<br class="hide show-on-portrait"/> gratis</em></td>
                         <td><em class="trzy-miesiace-gratis">+3 miesiące<br class="hide show-on-portrait"/> gratis</em></td>
                         <td><em class="trzy-miesiace-gratis">+3 miesiące<br class="hide show-on-portrait"/> gratis</em></td>
                         
                    </tr>
                    <tr>
                         <td>wystawianie faktur</td>
                         <td><em class="yes">tak</em></td>
                         <td><em class="yes">tak</em></td>
                         <td><em class="yes">tak</em></td>
                    </tr>
                    <tr>
                         <td>rejestrowanie kosztów</td>
                         <td><em class="yes">tak</em></td>
                         <td><em class="yes">tak</em></td>
                         <td><em class="yes">tak</em></td>
                    </tr>
                    <tr>
                         <td>OCR dokumentów</td>
                         <td><em class="opis ocr-podstawowy">5 stron/mc</em></td>
                         <td><em class="opis ocr-pelny">5 stron/mc</em></td>
                         <td><em class="opis">10 stron/mc</em></td>
                    </tr>
                    <tr>
                         <td>raporty, analizy</td>
                         <td><em class="yes">tak</em></td>
                         <td><em class="yes">tak</em></td>
                          <td><em class="yes">tak</em></td>
                    </tr>
                    <tr>
                         <td>aplikacje mobilne <em class="tooltip" title="Android, iOS">Więcej</em></td>
                         <td><em class="yes">tak</em></td>
                         <td><em class="yes">tak</em></td>
                         <td><em class="yes">tak</em></td>
                    </tr>
                    <tr>
                         <td>automatyczna windykacja</td>
                         <td><em class="yes">tak</em></td>
                         <td><em class="yes">tak</em></td>
                         <td><em class="yes">tak</em></td>
                    </tr>
                    <tr>
                         <td>faktury w walucie obcej</td>
                         <td><em class="yes">tak</em></td>
                         <td><em class="yes">tak</em></td>
                         <td><em class="yes">tak</em></td>
                    </tr>
                    <tr>
                         <td>logo firmy na fakturze</td>
                         <td><em class="yes">tak</em></td>
                         <td><em class="yes">tak</em></td>
                        <td><em class="yes">tak</em></td>
                    </tr>
                    <tr>
                         <td>faktury cykliczne</td>
                         <td><em class="yes">tak</em></td>
                         <td><em class="yes">tak</em></td>
                         <td><em class="yes">tak</em></td>
                    </tr>
                    <tr>
                         <td>automatycznie pobieranie kursu z NBP</td>
                         <td><em class="yes">tak</em></td>
                         <td><em class="yes">tak</em></td>
                         <td><em class="yes">tak</em></td>
                    </tr>

                    <tr>
                         <td>możliwość importu listy towarów</td>

                         <td><em class="yes">tak</em></td>
                         <td><em class="yes">tak</em></td>
                         <td><em class="yes">tak</em></td>
                    </tr>
                    <tr>
                         <td>tworzenie bazy asortymentu i kontrahentów</td>

                         <td><em class="yes">tak</em></td>
                         <td><em class="yes">tak</em></td>
                         <td><em class="yes">tak</em></td>
                    </tr>
                    <tr>
                         <td>magazyn</td>
                         <td></td>
                         <td><em class="yes">tak</em></td>
                         <td><em class="yes">tak</em></td>
                    </tr>
                    <tr>
                         <td>delegacje</td>
                         <td></td>
                         <td></td>
                         <td><em class="yes">tak</em></td>
                    </tr>
                    <tr>
                         <td>kadry</td>
                         <td></td>
                         <td></td>
                         <td><em class="yes">tak</em></td>
                    </tr>
                    <tr>
                         <td>płace</td>
                         <td></td>
                         <td></td>
                         <td><em class="yes">tak</em></td>
                    </tr>
                    <tr>
                         <td>amortyzacja ŚT</td>
                         <td></td>
                         <td></td>
                         <td><em class="yes">tak</em></td>
                    </tr>
                    <tr>
                         <td>kilometrówka</td>
                         <td></td>
                         <td></td>
                         <td><em class="yes">tak</em></td>
                    </tr>
                    <tr>
                         <td>deklaracje podatkowe <em class="tooltip" title="VAT-7, VAT-7K, VAT-UE, PIT-5, PIT-5L, PIT-11, PIT-4R">Więcej</em></td>
                         <td></td>
                         <td></td>
                         <td><em class="yes">tak</em></td>
                    </tr>
                    <tr>
                         <td>deklaracje ZUS <em class="tooltip" title="ZZA, ZUA, ZWUA, RCA, DRA">Więcej</em></td>
                         <td></td>
                         <td></td>
                         <td><em class="yes">tak</em></td>
                    </tr>
                    <tr>
                         <td>amortyzacja środków trwałych</td>
                         <td></td>
                         <td></td>
                         <td><em class="yes">tak</em></td>
                    </tr>
                    <tr>
                         <td>rejestry VAT</td>
                         <td></td>
                         <td></td>
                         <td><em class="yes">tak</em></td>
                    </tr>
                    <tr>
                         <td>KPiR</td>
                         <td></td>
                         <td></td>
                         <td><em class="yes">tak</em></td>
                    </tr>
                    <tr>
                         <td>obieg dokumentów</td>
                         <td></td>
                         <td></td>
                         <td><em class="yes">tak</em></td>
                    </tr>
                    <tr>
                         <td>komunikacja z księgowym</td>
                         <td></td>
                         <td></td>
                         <td><em class="yes">tak</em></td>
                    </tr>
                    <tr>
                         <td>eksport do programów księgowych <em class="tooltip" title="Comarch Optima, Sage Symfonia, Assecco WAPRO, FaKiR, KaPeR, Insert, Lefthand, Raks SQL, Fakt, R2fk">Więcej</em></td>
                         <td></td>
                         <td></td>
                         <td><em class="yes">tak</em></td>
                    </tr>
                    <tr>
                         <td></td>
                         <td class="center buy-td"><a href="https://app.szybkafaktura.pl/auth/new-register?app=skto" class="medium button green signup newButtonsignup">Wybierz</a></td>
                         <td class="center buy-td"><a href="https://app.szybkafaktura.pl/auth/new-register?app=skto" class="medium button green signup newButtonsignup">Wybierz</a></td>
                         <td class="center buy-td"><a href="https://app.szybkafaktura.pl/auth/new-register?app=skto" class="medium button green signup newButtonsignup">Wybierz</a></td>

                    </tr>
               </table>

          </section>

          <section id="mobile-pricelist" class="hide show-on-phone">

               <div class="options">

                    <h3>Wybierz okres <br class="hide show-on-tablet show-on-portrait"/>trwania abonamentu</h3>

                    <div class="option">
                         <label for="one-month">
                              <input type="radio" name="radio" id="one-month" value="one-month"/>
                              <figure></figure>
                              <span><big>1</big>miesiąc</span>
                         </label>
                    </div>
                    <div class="option">
                         <label for="three-months">
                              <input type="radio" name="radio" id="three-months" value="three-months"/>
                              <figure></figure>
                              <span><big>3</big>miesiące</span>
                         </label>
                    </div>
                    <div class="option">
                         <label for="six-months">
                              <input type="radio" name="radio" id="six-months" value="six-months"/>
                              <figure></figure>
                              <span><big>12</big>miesięcy</span>
                         </label>
                    </div>
                    <div class="option">
                         <label for="twelve-months">
                              <input type="radio" name="radio" id="twelve-months" value="twelve-months"/>
                              <figure></figure>
                              <span><big>24</big>miesiące</span>
                         </label>
                    </div>

               </div>

               <div id="mobile-packages">

                    <div class="package-header pakiet-podstawowy">
                         <div class="header-name">Pakiet Podstawowy</div>
                         <span class="price"><em class="zlote">9</em>,<em class="grosze">99</em></span>
                         <span class="monthly">zł / mc</span>
                    </div>
                    <div class="package-details pakiet-podstawowy">
                         <table>
                              <tr class="trzy-gratis">
                                   <td><em class="trzy-miesiace-gratis">+3 miesiące<br class="hide show-on-portrait"/> gratis</em></td>
                              </tr>
                              <tr>
                                   <td>wystawianie faktur</td>
                              </tr>
                              <tr>
                                   <td>rejestrowanie kosztów</td>
                              </tr>
                              <tr>
                                   <td>OCR dokumentów <span class="ocr-podstawowy">5 stron/mc</span></td>
                              </tr>
                              <tr>
                                   <td>raporty, analizy</td>
                              </tr>
                              <tr>
                                   <td>aplikacje mobilne <em class="tooltip" title="Android, iOS">Więcej</em></td>
                              </tr>
                              <tr>
                                   <td>automatyczna windykacja</td>
                              </tr>
                              <tr>
                                   <td>magazyn <em class="tooltip" title="9,99 zł / miesiąc">opcja</em></td>
                              </tr>
                              <tr>
                                    <td class="center buy-td"><a href="https://app.szybkafaktura.pl/auth/new-register?app=skto" class="medium button green signup newButtonsignup">Wybierz</a></td>

                              </tr>
                         </table>
                    </div>

                    <div class="package-header pakiet-pelny">
                         <div class="header-name">Pakiet Pełny</div>
                         <span class="price"><em class="zlote">29</em>,<em class="grosze">99</em></span>
                         <span class="monthly">zł / mc</span>
                    </div>
                    <div class="package-details pakiet-pelny">
                         <table>
                              <tr class="trzy-gratis">
                                   <td><em class="trzy-miesiace-gratis">+3 miesiące<br class="hide show-on-portrait"/> gratis</em></td>
                              </tr>
                              <tr>
                                   <td>wystawianie faktur</td>
                              </tr>
                              <tr>
                                   <td>rejestrowanie kosztów</td>
                              </tr>
                              <tr>
                                   <td>OCR dokumentów <span class="ocr-pelny">10 stron/mc</span></td>
                              </tr>
                              <tr>
                                   <td>raporty, analizy</td>
                              </tr>
                              <tr>
                                   <td>aplikacje mobilne <em class="tooltip" title="Android, iOS">Więcej</em></td>
                              </tr>
                              <tr>
                                   <td>automatyczna windykacja</td>
                              </tr>
                              <tr>
                                   <td>magazyn</td>
                              </tr>
                              <tr>
                                   <td>delegacje</td>
                              </tr>
                              <tr>
                                   <td>kadry</td>
                              </tr>
                              <tr>
                                   <td>płace</td>
                              </tr>
                              <tr>
                                   <td>amortyzacja ŚT</td>
                              </tr>
                              <tr>
                                   <td>kilometrówka</td>
                              </tr>
                              <tr>
                                   <td>deklaracje podatkowe <em class="tooltip" title="VAT-7, VAT-7K, VAT-UE, PIT-5, PIT-5L, PIT-11, PIT-4R">Więcej</em></td>
                              </tr>
                              <tr>
                                   <td>deklaracje ZUS <em class="tooltip" title="ZZA, ZUA, ZWUA, RCA, DRA">Więcej</em></td>
                              </tr>
                              <tr>
                                   <td>rejestry VAT</td>
                              </tr>
                              <tr>
                                   <td>KPiR</td>
                              </tr>
                              <tr>
                                   <td>obieg dokumentów</td>
                              </tr>
                              <tr>
                                   <td>cashflow <em class="tooltip" title="raport kasowy, wydruki kasowe, import wyciągów bankowych, saldo rachunków, raport zarządzania gotówką">Więcej</em></td>
                              </tr>
                              <tr>
                                   <td class="center buy-td"><a href="https://app.szybkafaktura.pl/auth/new-register?app=skto" class="medium button green signup newButtonsignup">Wybierz</a></td>

                              </tr>
                         </table>
                    </div>

                    <div class="package-header pakiet-all-inclusive">
                         <div class="header-name">Pakiet księgowości<br>All Inclusive</div>
                         <span class="price"><em class="from">od</em><em class="zlote">149</em>,<em class="grosze">00</em></span>
                         <span class="monthly">zł / mc</span>
                    </div>
                   <div class="package-details pakiet-all-inlusive">
                         <table>
                              <tr class="trzy-gratis">
                                <td><em class="trzy-miesiace-gratis">+ 3 miesiące<br class="hide show-on-portrait"/> gratis</em></td>
                              </tr>
                              <tr>
                                   <td>wystawianie faktur</td>
                              </tr>
                              <tr>
                                   <td>rejestrowanie kosztów</td>
                              </tr>
                              <tr>
                                   <td>OCR dokumentów bez ograniczeń</td>
                              </tr>
                              <tr>
                                   <td>raporty, analizy</td>
                              </tr>
                              <tr>
                                   <td>aplikacje mobilne <em class="tooltip" title="Android, iOS">Więcej</em></td>
                              </tr>
                              <tr>
                                   <td>automatyczna windykacja</td>
                              </tr>
                              <tr>
                                   <td>magazyn</td>
                              </tr>
                              <tr>
                                   <td>delegacje</td>
                              </tr>
                              <tr>
                                   <td>kadry</td>
                              </tr>
                              <tr>
                                   <td>płace</td>
                              </tr>
                              <tr>
                                   <td>amortyzacja ŚT</td>
                              </tr>
                              <tr>
                                   <td>kilometrówka</td>
                              </tr>
                              <tr>
                                   <td>deklaracje podatkowe <em class="tooltip" title="VAT-7, VAT-7K, VAT-UE, PIT-5, PIT-5L, PIT-11, PIT-4R">Więcej</em></td>
                              </tr>
                              <tr>
                                   <td>deklaracje ZUS <em class="tooltip" title="ZZA, ZUA, ZWUA, RCA, DRA">Więcej</em></td>
                              </tr>
                              <tr>
                                   <td>rejestry VAT</td>
                              </tr>
                              <tr>
                                   <td>KPiR</td>
                              </tr>
                              <tr>
                                   <td>obieg dokumentów</td>
                              </tr>
                              <tr>
                                   <td>cashflow <em class="tooltip" title="raport kasowy, wydruki kasowe, import wyciągów bankowych, saldo rachunków, raport zarządzania gotówką">Więcej</em></td>
                              </tr>
                              <tr>
                                   <td>komunikacja z księgowym</td>
                              </tr>
                              <tr>
                                   <td>eksport do programów księgowych <em class="tooltip" title="Comarch Optima, Sage Symfonia, Assecco WAPRO, FaKiR, KaPeR">Więcej</em></td>
                              </tr>
                              <tr>
                                   <td class="center buy-td"><a href="https://app.szybkafaktura.pl/auth/new-register?app=skto" class="medium button green signup newButtonsignup">Wybierz</a></td>

                              </tr>
                         </table>
                    </div>

               </div>

          </section>

          <div class="extra-packages">

               <h3>Dodatkowe paczki dokumentów</h3>
               <ul>
                    <li>50 stron - 6,99 zł netto</li>
                    <li>100 stron - 12,99 zł netto</li>
                    <li>500 stron - 59,99 zł netto</li>
               </ul>

          </div>

     </div>
</div>

<?php include_once("includes/footer.php") ?>

<script>
     (function (b, o, i, l, e, r) {
          b.GoogleAnalyticsObject = l;
          b[l] || (b[l] =
                  function () {
                       (b[l].q = b[l].q || []).push(arguments)
                  });
          b[l].l = +new Date;
          e = o.createElement(i);
          r = o.getElementsByTagName(i)[0];
          e.src = '//www.google-analytics.com/analytics.js';
          r.parentNode.insertBefore(e, r)
     }(window, document, 'script', 'ga'));
     ga('create', 'UA-1326744-3');
     ga('send', 'pageview');
</script>
</body>
</html>
